package dbapp;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum DBControllor{
    DBC;
	 private Connection conn = null;
	 private Statement stmt = null;
	 public ResultSet rs = null;
         private String usr = null;
         private String pwd = null;
         
        public void setinfo(String usr, String pwd){
            this.usr = usr;
            this.pwd = pwd;
        }

        public void connect() throws SQLException,ClassNotFoundException{
            try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("cannot find OracleDriver");
            }
            conn = DriverManager.getConnection("jdbc:oracle:thin:@fourier.cs.iit.edu:1521:ORCL",usr,pwd);
            stmt =  conn.createStatement(); 

        } 
        
       public void execut(String query) throws SQLException{
            rs = stmt.executeQuery(query);
            
        }
       
       public void close() throws SQLException{
           conn.close();
       }

        
       public void printResult() throws SQLException{
           
            ResultSetMetaData rsmd = rs.getMetaData();
            while(rs.next()){ 
                for(int x = 1 ;x<=rsmd.getColumnCount();x++){
                    if(x==rsmd.getColumnCount()){
                        System.out.println(rs.getString(x) + " "); 
                    }
                    else
                    {
                        System.out.print(rs.getString(x) + " "); 
                    }
                }
            }
        }
}
