/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dbapp;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author xunshuairu
 */
public class ConditionClause {
    private LinkedList<SingleClause> list = new LinkedList();
    
    public void add(SingleClause SC){
        list.add(SC);
    }
    
    public void clear(){
        list.clear();
    }
    
    public void informationCombine(){
        for(int x =0,y=1;;x++,y++){
            if(list.size()-1>=y){
                SingleClause SC1 = list.get(x);
                SingleClause SC2 = list.get(y);
                if((SC1.type==91||SC1.type==93)&&(SC2.type==91||SC2.type==93)){
                    SC1.value2 = SC2.value1;
                    list.remove(y);
                }
            }
            else{
                break;
            }
        }
    }
    
    
    public String toString(){
        //informationCombine();
        
        String out = "";
       
        for(SingleClause EE: list){
            if(EE.toString().equals("")){
                
            }
            else{
                if(!out.equals("")){
                    out = out +" and "+EE.toString();
                }
                else{
                    out = EE.toString();
                }
            }
        }
        return out;
    }
}
