/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dbapp;

/**
 *
 * @author xunshuairu
 */
public class SingleClause {
    public int type;
    public String attribute = null;
    
    public String value1 =null;
    public String value2 =null;
    
    public String toString(){
        String out="";
        if(attribute!=null){
            if(!attribute.isEmpty()){
                out=out+attribute;
            }
        }
        else{
            return "";
        }
        if(type==5||type==3||type==2||type==4||type==-6){
            if(value1!=null&&!value1.isEmpty()){
                out = out +"="+value1;
                return out;
            }
        }
        else if(type == 12||type==1){
            if(value1!=null&&!value1.isEmpty()){
                out = out +" like "+"'%"+value1+"%'";
                return out;
            }
        }
        else if(type==91||type==93){
             if(value1!=null&&!value1.isEmpty()){
                 if(value2!=null&&!value2.isEmpty()){
                    out = out+" >= date "+"'"+value1+"'"+" and " +attribute+" <= "+"date '"+value2+"'";
                    return out;
                 }
                 else if(value1!=null&&!value1.isEmpty()){
                     out = out+" = date "+"'"+value1+"'";
                     return out;
                 }
            }
        
        }
        return "";
    }
    
}
