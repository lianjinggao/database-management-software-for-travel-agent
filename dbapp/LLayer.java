/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dbapp;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Types;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import GUI.AddClient;

/**
 *
 * @author Shuairu
 */
public enum LLayer {// It is the logic layer of this application. Only one LLayer exist!
    LL;
    public int state_id;
    public String state;
    //<didn't use yet.>
    public LinkedList<String> states;
    
    public String id_slot1, id_slot2, id_slot3, id_slot4, id_slot5;
    public String name_slot1, name_slot2, name_slot3;
    public String single_slot1,single_slot2;// for gender position.
    public String zipCode_slot;
    public String phone_slot,phone_slot2;
    public String email_slot1,email_slot2;
    public String rating_slot;// for sun_rating
    public String price_slot1,price_slot2,price_slot3,price_slot4;
    public String date_slot1, date_slot2, date_slot3;
    
    public LinkedList<String> slots1 = new LinkedList();
    public ConditionClause clause = new ConditionClause();    
    private LinkedList<String> sqls;
    
    private boolean priv = true;
    
    public boolean exflag = true;
    
    
    private void getqotes(){
        if(name_slot1!=null){
            if(!name_slot1.isEmpty()){
                name_slot1 = "'"+name_slot1+"'";
            }
        }
        
        if(name_slot2!=null){
            if(!name_slot2.isEmpty()){
                name_slot2 = "'"+name_slot2+"'";
            }
        }
        
        if(name_slot3!=null){
            if(!name_slot3.isEmpty()){
             name_slot3 = "'"+name_slot3+"'";
            }
        }
        
        if(single_slot1!=null){
            if(!single_slot1.isEmpty()){
                single_slot1 = "'"+single_slot1.toLowerCase()+"'";
            }
        }
        if(single_slot2!=null){
            if(!single_slot2.isEmpty()){
                single_slot2 = "'"+single_slot2.toLowerCase()+"'";
            }
        }
        if(email_slot1!=null){
            if(!email_slot1.isEmpty()){
                email_slot1 = "'"+email_slot1+"'";
            }
        }
        if(email_slot2!=null){
            if(!email_slot2.isEmpty()){
                email_slot2 = "'"+email_slot2+"'";
            }
        }
    }

   public void refreshSlots(){
        id_slot1=null; id_slot2=null; id_slot3=null; id_slot4=null;
        name_slot1 = null; name_slot2 = null; name_slot3 = null;
        single_slot1=null; single_slot2=null;
        zipCode_slot = null;
        phone_slot = null; phone_slot2 = null;
        email_slot1 = null; email_slot2 = null;
        rating_slot = null;
        price_slot1 = null; price_slot2 = null; price_slot3 = null; price_slot4 = null;
        date_slot1 = null; date_slot2 = null;
        slots1.clear();
        clause.clear();
    }
    
    public void refreshQury(){
        sqls.clear();
    }
    
    
    public boolean checkInfo(){
        if(state_id == 1||state_id==2){
            if(name_slot1!=null&&!name_slot1.isEmpty()
            		&&name_slot2!=null&&!name_slot2.isEmpty()){
               return checkName();
            }
        }
        return false;
    }
    
  	private boolean checkID(){
  		
  		return id_slot1.length() <= 10 
  				&& id_slot2.length() <= 10 
  				&& id_slot1.matches("^[0-9]*")
  				&& id_slot2.matches("^[0-9]*");
  	}
  	
  	private boolean checkName(){
  		return  
  				 name_slot1.matches("^[a-zA-Z ]*")
  				&& name_slot2.matches("^[a-zA-Z ]*");
  	}
        
        private boolean checkName(String someslot, int requiredlength){
  		return   someslot.length()<=requiredlength
                        &&someslot.matches("^[a-zA-Z ]*");
  				
  	}
  	
  	private boolean checkZipcode(){
  		return zipCode_slot.length() <= 5 
  				&& zipCode_slot.matches("^[0-9]*");
  	}
  	
  	private boolean checkPhone(){
  		return phone_slot.length() <= 15
  				&& phone_slot2.length() <= 15
  				&& phone_slot.matches("^[0-9]*")
  				&& phone_slot2.matches("^[0-9]*");
  	}
  	
  	private boolean checkEmail(){
  		return email_slot1.length() <= 40
  				&& email_slot2.length() <= 40
  				&& email_slot1.contains("@")
  				&& email_slot2.contains("@");
  	}
  	
  	private boolean checkRating(){
  		return rating_slot.equals("1")
  				|| rating_slot.equals("2")
  				|| rating_slot.equals("3");
  	}
  	
  	private boolean checkPrice(){
  		return price_slot1.length() <= 7
  				&& price_slot1.matches("^[0-9.]*")
  				&& price_slot1.lastIndexOf(".") == price_slot1.indexOf(".");
  	}
  	//mm-dd-yyyy
  	private boolean checkDate(){
  		String []date1 = date_slot1.split("-");
  		String []date2 = date_slot2.split("-");
  		return date_slot1.matches("^[0-9-]*")
  				&& date1[0].length() <= 2 && date1[0].length() >= 1
  				&& date1[1].length() <= 2 && date1[1].length() >= 1
  				&& date1[2].length() == 4
  				&& date2[0].length() <= 2 && date2[0].length() >= 1
  				&& date2[1].length() <= 2 && date2[1].length() >= 1
  				&& date2[2].length() == 4;
  	}
  	
  	private boolean checkSingle(String a){
  		if(a.equals("gender"))
  			return (single_slot1.equals("m") || single_slot1.equals("f"))
  					&& (single_slot2.equals("m") || single_slot2.equals("f"));
  		else if(a.equals("position"))
  			return (single_slot1.equals("m") || single_slot1.equals("a"))
  					&& (single_slot2.equals("m") || single_slot2.equals("a"));
  		else if(a.equals("boolean"))
  			return (single_slot1.equals("true") || single_slot1.equals("false"))
  					&& (single_slot2.equals("true") || single_slot2.equals("false"));
  		else
  			return false;			
  	}    
   
    public void setStateFresh(String input){
        setState(input);
        this.exflag = true;
        refreshSlots();
        refreshQury();
    }
    
        
        
    public void setState(String input){
        if(input.toLowerCase().equals("addclient")||
                input.toLowerCase().equals("queryclient")){
            state_id = 1;
            state = "person";
        }
        else if(input.toLowerCase().equals("addcity")||
                input.toLowerCase().equals("querycity")||
                input.toLowerCase().equals("city")){
            state_id = 2;
            state = "city";
        }
        else if(input.toLowerCase().equals("addnewamenity")||
                input.toLowerCase().equals("queryamenity")){
        	state_id = 3;
            state = "amenity";
        }
        else if(input.toLowerCase().equals("addresort")||
                input.toLowerCase().equals("queryresort")){
            state_id = 4;
            state = "resort";
        }
        else if(input.toLowerCase().equals("addroomtype")||
                input.toLowerCase().equals("queryroomtype")){
        	state_id = 5;
            state = "room";
        }
        else if(input.toLowerCase().equals("addbooking")||
                input.toLowerCase().equals("querybooking")){
        	state_id = 6;
            state = "booking";
        }
        else if(input.toLowerCase().equals("addamenity")){
        	state_id = 7;
            state = "amenity";
        }
        else if(input.toLowerCase().equals("trip_payment")){
        	state_id = 8;
            state = "trip_payment";
        }
        else if(input.toLowerCase().equals("trip_payment_name")){//view query
        	state_id = 9;
            state = "trip_payment_name";
        }
        else if(input.toLowerCase().equals("queryperson")){
            state_id = 22;
            state = "person";
        }
        else if(input.toLowerCase().equals("queryagent")){
            state_id = 10;
            state = "agent";
        }
        else if(input.toLowerCase().equals("email")){
            state_id = 9;
            state = "email";
        }
        else if(input.toLowerCase().equals("person_phone")){
            state_id = 9;
            state = "person_phone";
        }
        else if(input.toLowerCase().equals("bookingresortid")){
            state_id = 9;
            state = "trip_resort";
        }
        else if(input.toLowerCase().equals("bookingresortname")){
            state_id = 9;
            state = "booking_resort_name";
        }
        else if(input.toLowerCase().equals("clienttrip")){
            state_id = 9;
            state = "clienttrip";
        }
        else if(input.toLowerCase().equals("totalrevenueagent")){
            state_id = 9;
            state = "totalrevenueagent";
        }
        else if(input.toLowerCase().equals("totalrevenuecity")){
            state_id = 9;
            state = "totalrevenuecity";
        }
        else if(input.toLowerCase().equals("totalrevenuecountry")){
            state_id = 9;
            state = "totalrevenuecountry";
        }
        else if(input.toLowerCase().equals("resortamenitylist")){
            state_id = 9;
            state = "resortamenitylist";
        }
        else if(input.toLowerCase().equals("resortamenitycount")){
            state_id = 9;
            state = "resortamenitycount";
        }
        else if(input.toLowerCase().equals("resortcity")){
            state_id = 9;
            state = "resortcity";
        }
        else if(input.toLowerCase().equals("resortcountry")){
            state_id = 9;
            state = "resortcountry";
        }
        else if(input.toLowerCase().equals("clientresortdays")){
            state_id = 9;
            state = "clientresortdays";
        }
        else if(input.toLowerCase().equals("clientresortdayssum")){
            state_id = 9;
            state = "clientresortdayssum";
        }
        else if(input.toLowerCase().equals("numberoftrips")){
            state_id = 9;
            state = "numberoftrips";
        }
        else if(input.toLowerCase().equals("avgdayspertrip")){
            state_id = 9;
            state = "avgdayspertrip";
        }
        else if(input.toLowerCase().equals("agenthandledtrips")){
            state_id = 9;
            state = "agenthandledtrips";
        }
        else if(input.toLowerCase().equals("agenthandledtripscount")){
            state_id = 9;
            state = "agenthandledtripscount";
        }
        else if(input.toLowerCase().equals("booking_resort_name_price")){
            state_id = 9;
            state = "booking_resort_name_price";
        }
        else if(input.toLowerCase().equals("agenthandledclientscount")){
            state_id = 9;
            state = "agenthandledclientscount";
        }
        else if(input.toLowerCase().equals("resortallamenity")){
            state_id = 9;
            state = "resortallamenity";
        }
        else if(input.toLowerCase().equals("resorts_room_type")){
            state_id = 9;
            state = "resorts_room_type";
        }
        else if(input.toLowerCase().equals("14b")){
            state_id = 101;
            state = "          ";
        }
        else{//booking_resort_name_price
            System.out.println("need to add new state");
        }
        
    }
    
    public void genrateQuery() throws SQLException{
        if(state_id==1){

            personquery();
            
        }
        else if(state_id==2){
            cityaddquery();
        }
        else if(state_id==3){
        	addnewamenityquery();
        }
        else if(state_id==4){
        	resortaddquery();
        }
        else if(state_id==5){
            roomtypeaddquery();
        }
        else if(state_id==6){
            bookingaddquery();
        }
        else if(state_id==7){
        	addamenityquery();
        }
        else if(state_id==9){
        	call_view();
        }
        else if(state_id==10){
        	callpass();
        }
    }

    public void callpass() {
    	sqls.add("select id, poition from agent");
	}

	private void call_view() {
    	sqls.add("select * from " + state);
	}
//    public void callView(String tableName) {
//    	sqls.add("select * from " + tableName);
//	}

	public void genrateSearch() throws SQLException{
        String tmp =clause.toString();
        String components = "select * from "+ state +" where "+ tmp;
        if(tmp.equals(""))
        {
            components = "select * from "+ state + " order by 1";
        }
        
        sqls.add(components);
        System.out.println(components);
    }
    
    public void genrateQueryAdd() throws SQLException{
        
    
    }
    
    public void generateSimpleLook(){
        String components = "select * from " + state + " order by 1";
        sqls.add(components);
        System.out.println(components);
    }
    
    public void generateSimpleLookNoOrder(){
        String components = "select * from " + state;
        sqls.add(components);
        System.out.println(components);
    }
    
    public void genrateQueryUpdate() throws SQLException{
        DBControllor.DBC.execut("select * from " + state);
        ResultSetMetaData metaData = DBControllor.DBC.rs.getMetaData();
        int maxANum = metaData.getColumnCount();
        int types[] = new int[maxANum+1];
        LinkedList<String > attNames = new LinkedList();
        for(int x=0;x<maxANum ;x++){
            types[x] = metaData.getColumnType(x+1);
            attNames.add(metaData.getColumnName(x+1));
        }
        if((slots1.size()/2)==maxANum){
            for(int x=0;x<maxANum ;x++){
                String newS = modifyWithType(slots1.get(x), types[x]);
                slots1.set(x, newS);
            }
            int y=maxANum;
            for(int x=y;x<=slots1.size()-1 ;x++){
                String newS = modifyWithType(slots1.get(x), types[x-y]);
                slots1.set(x, newS);
            }
            
            for(int x=0;x<maxANum ;x++){
                String newS = bindingEquals(slots1.get(x), attNames.get(x));
                slots1.set(x, newS);
            }
            for(int x=y;x<=slots1.size()-1 ;x++){
                String newS = bindingEquals(slots1.get(x), attNames.get(x-y));
                slots1.set(x, newS);
            }
            System.out.println(slots1.toString());
            
            
            String conditionclause = connectingcondition(slots1.subList(0, y));//conditioon is from 0-y
            String setclause = connectingset(slots1.subList(y, slots1.size()));//ser is from y - max
            
            String components = " update "+state+" set "+ setclause + " where "+ conditionclause;
            sqls.add(components);
            System.out.println(components);
            
        }
        else{
            System.out.println("it possible, but not yet");
        }
        
        
    }
        private String connectingcondition(List<String> input){
            String output = "";
            if(input.size()>1){
                for(int x=0;x<=input.size()-2 ;x++){
                    output = output+input.get(x);
                    output = output+" and ";
                }
                output = output+input.get(input.size()-1);
            }
            return output;
        }
        
        private String connectingset(List<String> input){
            String output = "";
            if(input.size()>1){
                for(int x=0;x<=input.size()-2 ;x++){
                    output = output+input.get(x);
                    output = output+" , ";
                }
                output = output+input.get(input.size()-1);
            }
            return output; 
        }
    
        private String modifyWithType(String input,int type){
            if(type == 12||type==1){
                input = "'"+input+"'";
            }
            else if(type ==2||type==3||type==5){
            
            }
            else if(type == 93||type == 91){
                input =  " date '"+input+"'";
            }
            else{
                System.out.println("new type ourred please check your program."+type);
            }
            return input;
        }
    
        private String bindingEquals(String input,String typename){
            String newS = typename+"="+input;
            return newS;
        }
    
    
    public void genrateQueryDelete() throws SQLException{
       DBControllor.DBC.execut("select * from " + state);
        ResultSetMetaData metaData = DBControllor.DBC.rs.getMetaData();
        int maxANum = metaData.getColumnCount();
        int types[] = new int[maxANum+1];
        LinkedList<String > attNames = new LinkedList();
        for(int x=0;x<maxANum ;x++){
            types[x] = metaData.getColumnType(x+1);
            attNames.add(metaData.getColumnName(x+1));
        }
        if((slots1.size())==maxANum){
            for(int x=0;x<maxANum ;x++){
                String newS = modifyWithType(slots1.get(x), types[x]);
                slots1.set(x, newS);
            }
          
            
            for(int x=0;x<maxANum ;x++){
                String newS = bindingEquals(slots1.get(x), attNames.get(x));
                slots1.set(x, newS);
            }

            System.out.println(slots1.toString());
            String conditionclause = connectingcondition(slots1.subList(0, maxANum));
            
            String components = " delete from "+ state + " where "+ conditionclause;
            sqls.add(components);
            System.out.println(components);
            
        }
        else{
            System.out.println("it possible, but not yet");}
        }

        private void personquery() throws SQLException{//1
            formatDate();
            String newID = idgenerator("id","person");
            getqotes();//--------------------------------------------------------------------
            String components = "insert into person values ("+newID+","+name_slot1+","+name_slot2+","+date_slot1+","+single_slot1+","+zipCode_slot+ ")";
            System.out.println(components);
            sqls.add(components);
            if(single_slot2!=null){
                if(!single_slot2.isEmpty()){
                    agentquery(newID);
                }
            }
            if(phone_slot!=null){
                if(!phone_slot.isEmpty()){
                    phoneaddquery(newID,phone_slot);
                }
            }
            if(phone_slot2!=null){
                if(!phone_slot2.isEmpty()){
                    phoneaddquery(newID,phone_slot2);
                }
            }
            if(email_slot1!=null){
                if(!email_slot1.isEmpty()){
                    emailaddquery(newID,email_slot1);
                }
            }
            if(email_slot2!=null){
                if(!email_slot2.isEmpty()){
                    emailaddquery(newID,email_slot2);
                }
            }
        }

			private void agentquery(String newID){
	            String components = "insert into agent values ("+newID+","+single_slot2+","+name_slot3+")";
	            System.out.println(components);
	            sqls.add(components);
	        }
	        
	        private void emailaddquery(String newID, String mailAdd){
	            String components = "insert into Email values ("+newID+","+mailAdd+ ")";
	            System.out.println(components);
	            sqls.add(components);
	        }
	        
	        private void phoneaddquery(String newID, String phoneNum){
	            String components = "insert into person_phone values ("+newID+","+phoneNum+ ")";
	            System.out.println(components);
	            sqls.add(components);
	        }
        
		private void cityaddquery() throws SQLException{//2
		    getqotes();
		    String components = "insert into city values ("+name_slot1+","+name_slot2+ ")";
		    System.out.println(components);
		    //sqlquery = components;
		    sqls.add(components);
		}
		
		private void addnewamenityquery() throws SQLException {//3
			getqotes();
			String newID = idgenerator("amenity_id","amenity");
			String components = "insert into amenity values("+newID+","+name_slot1+")";
			System.out.println(components);
			sqls.add(components);			
		}
		
		private void resortaddquery() throws SQLException{//4
		    getqotes();
		    String newID = idgenerator("resort_id","resort");
		    System.out.println(newID);
		    String components = "insert into resort values ("+newID+","+name_slot1+","+name_slot2+","+name_slot3+
		    		","+ phone_slot+","+rating_slot+")";
		    System.out.println(components);
		    sqls.add(components);
		    if(id_slot1!=null && !id_slot1.isEmpty()){
		    	addResortRoomQuery(newID,id_slot1,price_slot1);
            }
            if(id_slot2!=null && !id_slot2.isEmpty()){
            	addResortRoomQuery(newID,id_slot2,price_slot2);
            }
            if(id_slot3!=null && !id_slot3.isEmpty()){
            	addResortRoomQuery(newID,id_slot3,price_slot3);
            }
            if(id_slot4!=null && !id_slot4.isEmpty()){
            	addResortRoomQuery(newID,id_slot4,price_slot4);
            }
		}
			private void addResortRoomQuery(String resort_id, String room_type_id, String price)
			{
				String components = "insert into resort_room values("+resort_id+","+room_type_id+","+price+")";
				System.out.println(components);
				sqls.add(components);
			}
		 
		private void roomtypeaddquery() throws SQLException{//5
			getqotes();
			String newID = idgenerator("room_type_id","room");
			String components = "insert into room values("+newID+","+name_slot1+")";
			System.out.println(components);
			sqls.add(components);
		}
		   
		
		private void bookingaddquery() throws SQLException {//6
			getqotes();
			formatDate();
			if(id_slot5!= null && !id_slot5.isEmpty()){
				bookingDateQuery(id_slot5,id_slot1,id_slot3,id_slot4,rating_slot,date_slot2,date_slot3);
			}
			else{
				String newID = idgenerator("trip_id","trip_payment");
				String components = "insert into trip_payment values("+newID+","+id_slot1+","+id_slot2+","+date_slot1+")";
				System.out.println(components);
				sqls.add(components);
				bookingDateQuery(newID,id_slot1,id_slot3,id_slot4,rating_slot,date_slot2,date_slot3);
			}
				
		}
			
			private void bookingDateQuery(String trip_id, String person_id, String resort_id, String room_type_id, 
					String room_quantity, String arrival_date,String departure_date) {
				formatDate();
				String components = "insert into trip_resort values("+trip_id+","+person_id+","+resort_id+","+
					room_type_id+","+room_quantity+","+arrival_date+","+departure_date+")";
				System.out.println(components);
				sqls.add(components);
			}

		
		private void addamenityquery() {//7
			String components = "insert into resort_amenity values("+id_slot1+","+id_slot2+")";
			System.out.println(components);
			sqls.add(components);
		}
        
	public void formatDate(){
            if(date_slot1!=null){
                if(!date_slot1.isEmpty()){
                    date_slot1 = "to_date('"+date_slot1+ "','mm-dd-yyyy')";
                }
            }
            if(date_slot2!=null){
                if(!date_slot2.isEmpty()){
                    date_slot2 = "to_date('"+date_slot2+ "','mm-dd-yyyy')";
                }
            }
            if(date_slot3!=null){
                if(!date_slot3.isEmpty()){
                    date_slot3 = "to_date('"+date_slot3+ "','mm-dd-yyyy')";
                }
            }
        }
		
            public String idgenerator(String Attribute ,String tablename) throws SQLException{
        		LinkedList<Integer> IDS = new LinkedList<Integer>();
                DBControllor.DBC.execut("select "+Attribute+" from "+ tablename+ " order by "+Attribute);
                while(DBControllor.DBC.rs.next()){
                    String ff = DBControllor.DBC.rs.getString(1);
                    Integer newI = new Integer(ff);
                    IDS.add(newI);
                }
                if(IDS.size()==0){
                    return "1";
                }
                for(int x= 1;x<=IDS.size()+1 ;x++){
                    boolean flag = false;
                    for(Integer EE: IDS){
                        if(EE.intValue()==x){
                            flag = true;
                            break;
                        }
                    }
                    if(flag==false){
                        return Integer.valueOf(x).toString();
                    }
                }
                System.out.println("logic error");
                return null;
            }

    public void specialcase1sql() throws SQLException{
        
        String components = "select id, first_name, last_name, sum(total_price) from "
                + "(select * from money_spent_per_trip where booking_date between "
                + "date '"+LL.date_slot1 +"' and date '"+LL.date_slot2+"') group by first_name, last_name,id order by 4 desc";
        System.out.println(components);
        DBControllor.DBC.execut(components);
        this.refreshQury();
    }

    
    public void specialcase2sql() throws SQLException{
        String components =
                "select id, first_name, last_name, sum(number_of_trips) from "
                + "(select * from CountClientTrips where booking_date between "
                + "date '"+LL.date_slot1 +"' and date '"+LL.date_slot2+"') group by first_name, last_name,id order by 4 desc";
        System.out.println(components);
        DBControllor.DBC.execut(components);
        this.refreshQury();
    }
            
            
            
    private void searchcity(){
        name_slot1 = AddLikeSign(name_slot1);
        name_slot2 = AddLikeSign(name_slot2);
        String components = "select * from city where city like "+name_slot1+" and "+" country like "+name_slot2;
        sqls.add(components);
        System.out.println(components);
    }
    
    private String AddLikeSign(String input ){
        String output = "'%'";
        if(input!=null){
            if(!input.isEmpty()){
                output = "'%"+ input +"%'";
            }
        }
        return output;   
    }
            
            
    public void getConnect(){
        this.sqls = new LinkedList();
        
        try {
            DBControllor.DBC.setinfo("mcui1", "cmcm1234");
            DBControllor.DBC.connect();
        } catch (SQLException ex) {
            Logger.getLogger(LLayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LLayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    
    public void close() throws SQLException{
        DBControllor.DBC.close();
    
    }
    
    
    public void sentquery(){
        
    	if(sqls.size()!=0){
            for(String EE: sqls){
            	try{
                DBControllor.DBC.execut(EE);
            	}
            	catch(SQLException ex){
            		handle(ex);
                        this.LL.exflag = false;
                }
            }
        }
        this.refreshQury();
    }
    public class MessageBox extends JComponent{
        	
      }

    public void handle(SQLException ex){
		if(ex.getMessage().contains("02291"))
			JOptionPane.showMessageDialog(new MessageBox(), "Please check if some information exists.");
		if(ex.getMessage().contains("00936"))
			JOptionPane.showMessageDialog(new MessageBox(), "Please check if some blanks are empty.");
		else
			JOptionPane.showMessageDialog(new MessageBox(),ex.getMessage());
    }


	public boolean verifyLogin(String id, char [] password) throws SQLException {
		String inputPass = "";
		String realPass = "";
		for(int i = 0; i < password.length; i++)
			inputPass += String.valueOf(password[i]);
		DBControllor.DBC.execut("select * from (agent natural join person) where id = "+id);
		while (DBControllor.DBC.rs.next()){
			realPass = DBControllor.DBC.rs.getString(3);
                        if(DBControllor.DBC.rs.getString(2).equals("a")){
                            this.LL.priv=false;
                        }
                        else if(DBControllor.DBC.rs.getString(2).equals("m")){
                            this.LL.priv=true;
                        }
                }
                
		if(inputPass.equals(realPass))
			return true;
		return false;
	}
    
        private void addfakesql(){
        
            sqls.add("gjdshkfhjksdhkfh");
        }
    
        public boolean getpriv(){
            return this.LL.priv;
        }
    
}
