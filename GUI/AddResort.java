package GUI;

import dbapp.LLayer;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddResort extends JPanel{
	private JTextField name;
	private JTextField city;
	private JTextField address;
	private JTextField phone;
	private JComboBox sunRating;
	private int rating;
	private JTextField type1, type2, type3, type4, 
			price1, price2, price3, price4;
    
    public AddResort(){
    	//base
		JPanel base = new JPanel(new BorderLayout());
		
		//left
		JPanel left = new JPanel();
		left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
		
		JPanel firstRow = new JPanel(new FlowLayout());
		JPanel secondRow = new JPanel(new FlowLayout());
		JPanel thirdRow = new JPanel(new FlowLayout());
		JPanel fourthRow = new JPanel(new FlowLayout());
		
		JLabel nameLabel = new JLabel("Resort_Name:");
		JLabel cityLabel = new JLabel("City:");
		JLabel addressLabel = new JLabel("Address:");
		JLabel phoneLabel = new JLabel("Phone:");

				
		name = new JTextField(10);
		city = new JTextField(10);
		address = new JTextField(20);
		phone = new JTextField(10);
				
		firstRow.add(nameLabel);
		firstRow.add(name);
		
		secondRow.add(cityLabel);
		secondRow.add(city);
		secondRow.add(phoneLabel);
		secondRow.add(phone);
		
		thirdRow.add(addressLabel);
		thirdRow.add(address);

		JLabel sunRatingLabel = new JLabel("Sun_Rating:");
		String [] rate = {"1","2","3"};
		sunRating = new JComboBox(rate);
		sunRating.addActionListener(new Rating());

		fourthRow.add(sunRatingLabel);
		fourthRow.add(sunRating);
		
		
		left.add(firstRow);
		left.add(secondRow);
		left.add(thirdRow);
		left.add(fourthRow);
		
		base.add(left ,BorderLayout.LINE_START);
		
		
		//right
		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));
		
		JPanel firstRowR = new JPanel(new FlowLayout());
		JPanel secondRowR = new JPanel(new FlowLayout());
		JPanel thirdRowR = new JPanel(new FlowLayout());
		JPanel fourthRowR = new JPanel(new FlowLayout());
		JPanel fifthRowR = new JPanel(new FlowLayout());
		
		type1 = new JTextField(2);
		type2 = new JTextField(2);
		type3 = new JTextField(2);
		type4 = new JTextField(2);
		price1 = new JTextField(5);
		price2 = new JTextField(5);
		price3 = new JTextField(5);
		price4 = new JTextField(5);
		
		firstRowR.add(new JLabel("Room type id:"));
		firstRowR.add(type1);
		firstRowR.add(new JLabel("Price:"));
		firstRowR.add(price1);
		
		secondRowR.add(new JLabel("Room type id:"));
		secondRowR.add(type2);
		secondRowR.add(new JLabel("Price:"));
		secondRowR.add(price2);
		
		thirdRowR.add(new JLabel("Room type id:"));
		thirdRowR.add(type3);
		thirdRowR.add(new JLabel("Price:"));
		thirdRowR.add(price3);
		
		fourthRowR.add(new JLabel("Room type id:"));
		fourthRowR.add(type4);
		fourthRowR.add(new JLabel("Price:"));
		fourthRowR.add(price4);
		
		fifthRowR.add(new JLabel("Please use \"Add >> Room\" menu if you need to add more."));
		
		right.add(firstRowR);
		right.add(secondRowR);
		right.add(thirdRowR);
		right.add(fourthRowR);
		right.add(fifthRowR);

		base.add(right, BorderLayout.LINE_END);
		
		//bot
		JPanel bot = new JPanel(new FlowLayout());
		JButton submit = new JButton("Submit");
        submit.addActionListener(new AddAction());
		bot.add(submit);

		base.add(bot, BorderLayout.PAGE_END);
		
		this.add(base);
    }
    
    private class AddAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			LLayer.LL.setStateFresh("addresort");
			LLayer.LL.name_slot1 = name.getText();
			LLayer.LL.name_slot2 = city.getText();
			LLayer.LL.name_slot3 = address.getText();
			LLayer.LL.phone_slot = phone.getText();
			LLayer.LL.rating_slot = String.valueOf(rating);
			
			LLayer.LL.id_slot1 = type1.getText();
			LLayer.LL.id_slot2 = type2.getText();
			LLayer.LL.id_slot3 = type3.getText();
			LLayer.LL.id_slot4 = type4.getText();
			
			LLayer.LL.price_slot1 = price1.getText();
			LLayer.LL.price_slot2 = price2.getText();
			LLayer.LL.price_slot3 = price3.getText();
			LLayer.LL.price_slot4 = price4.getText();
			try {
	             LLayer.LL.genrateQuery();
	             LLayer.LL.sentquery();
                     JOptionPane.showMessageDialog(AddResort.this,"You have successfully added the resort information!");
			name.setText("");
			city.setText("");
			address.setText("");
			phone.setText("");
			sunRating.setSelectedIndex(0);
			type1.setText("");
			type2.setText("");
			type3.setText("");
			type4.setText("");
			price1.setText("");
			price2.setText("");
			price3.setText("");
			price4.setText("");
	         } catch (SQLException ex) {
	             Logger.getLogger(AddClient.class.getName()).log(Level.SEVERE, null, ex);
	         }
			
		}
    }
    
    private class Rating extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
	        JComboBox cb = (JComboBox)e.getSource();
	        rating = Integer.valueOf((String)cb.getSelectedItem());
	        System.out.println(rating);
		}
    }
}
