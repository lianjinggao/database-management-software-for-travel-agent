package GUI;
import javax.swing.*;

import dbapp.LLayer;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddAmenity extends JPanel{
	private JTextField resortText = new JTextField(10);
	private JTextField amenityText = new JTextField(5);
	private JTextField amenityNameText = new JTextField(10);
	
	public AddAmenity(){
		
		JPanel base = new JPanel(new BorderLayout());
		
		//baseLeft
		JPanel baseLeft = new JPanel();
		baseLeft.setLayout(new BoxLayout(baseLeft, BoxLayout.PAGE_AXIS));
		
		//left
		JPanel firstRow = new JPanel(new FlowLayout());
		firstRow.add(new JLabel("Resort_id:"));
		firstRow.add(resortText);
		baseLeft.add(firstRow);
		
		JPanel secondRow = new JPanel(new FlowLayout());
		secondRow.add(new JLabel("Amenity_id:"));
		secondRow.add(amenityText);
		baseLeft.add(secondRow);
		
		JPanel thirdRow = new JPanel(new FlowLayout());
		JButton add = new JButton("Add");
		add.addActionListener(new addAction());
		thirdRow.add(add);
		baseLeft.add(thirdRow);
		
		//baseRight
		JPanel baseRight = new JPanel();
		baseRight.setLayout(new BoxLayout(baseRight, BoxLayout.PAGE_AXIS));
		
		JPanel firstRowR = new JPanel(new FlowLayout());
		firstRowR.add(new JLabel("Add New Amenity:"));
		baseRight.add(firstRowR);
		
		JPanel secondRowR = new JPanel(new FlowLayout());
		secondRowR.add(new JLabel("Amenity_name:"));
		secondRowR.add(amenityNameText);
		baseRight.add(secondRowR);
		
		JPanel thirdRowR = new JPanel(new FlowLayout());
		JButton addR = new JButton("Add");
		addR.addActionListener(new addRAction());
		thirdRowR.add(addR);
		baseRight.add(thirdRowR);

		
		
		base.add(baseLeft, BorderLayout.LINE_START);
		base.add(baseRight, BorderLayout.LINE_END);
		this.add(base);
	}
	
	private class addAction extends AbstractAction{
		
		@Override
		public void actionPerformed(ActionEvent e) {
    		LLayer.LL.setStateFresh("addamenity");
    		LLayer.LL.id_slot1 = resortText.getText();
    		LLayer.LL.id_slot2 = amenityText.getText();
    		try {
                LLayer.LL.genrateQuery();
                LLayer.LL.sentquery();
                JOptionPane.showMessageDialog(AddAmenity.this,"You have successfully added the amenity information!");
    		resortText.setText("");
    		amenityText.setText("");
            } catch (SQLException ex) {
                Logger.getLogger(AddClient.class.getName()).log(Level.SEVERE, null, ex);
            }

		}
	}
	
	private class addRAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			LLayer.LL.setStateFresh("addnewamenity");
    		LLayer.LL.name_slot1 = amenityNameText.getText();
    		try {
                LLayer.LL.genrateQuery();
                LLayer.LL.sentquery();
            } catch (SQLException ex) {
                Logger.getLogger(AddClient.class.getName()).log(Level.SEVERE, null, ex);
            }			
		}	
	}
}
