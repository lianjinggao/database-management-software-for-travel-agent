package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryBookingName extends JPanel 
{
    private JTextField tripid = new JTextField(5);
    private JTextField cilentFirstName = new JTextField(5);
    private JTextField cilentLastName = new JTextField(5);
    private JTextField agentFirstName = new JTextField(5);
    private JTextField agentLastName = new JTextField(5);
    private JTextField bookingDate = new JTextField(6);
    private JTextField bookingDateTo = new JTextField(6);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
	
	public ComQueryBookingName() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("trip_payment_name",0, new Dimension(700,300));
        baseUpper.add(new JLabel("Trip ID:"));
        baseUpper.add(tripid);
        baseUpper.add(new JLabel("Client First Name:"));
        baseUpper.add(cilentFirstName);
        baseUpper.add(new JLabel("Client Last Name:"));
        baseUpper.add(cilentLastName);
        baseUpper.add(new JLabel("Agent First Name:"));
        baseUpper.add(agentFirstName);
        baseUpper.add(new JLabel("Agent Last Name:"));
        baseUpper.add(agentLastName);
        
        baseUpper2.add(new JLabel("Booking Date From:"));
        baseUpper2.add(bookingDate);
        baseUpper2.add(new JLabel("Booking Date To:"));
        baseUpper2.add(bookingDateTo);
        
        baseUpper3.add(new JLabel("Date Format: yyyy-mm-dd            "));
        baseUpper3.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper2);
        up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("trip_payment_name");
            
            LLayer.LL.slots1.add(tripid.getText());
            LLayer.LL.slots1.add(cilentFirstName.getText());
            LLayer.LL.slots1.add(cilentLastName.getText());
            LLayer.LL.slots1.add(agentFirstName.getText());
            LLayer.LL.slots1.add(agentLastName.getText());
            LLayer.LL.slots1.add(bookingDate.getText());
            LLayer.LL.slots1.add(bookingDateTo.getText());
            //LLayer.LL.slots1.add(bookingDateTo.getText());
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }	
}