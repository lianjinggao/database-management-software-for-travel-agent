/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import dbapp.*;
import java.awt.*;
import static java.awt.image.ImageObserver.WIDTH;
import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Shuairu
 */
public class GridTable extends JPanel {
    
    public static final Dimension DEFAULT_DIMENSION = new Dimension(200,300);
    private Dimension recordDim = new Dimension();
    private JTable table;
    private ListSelectionModel listSelectionModel;
    private Vector<Vector<Object>> data;
    private Vector<String> columnNames;
    private int columntypes[];
    
    private SQLException ex;
    private String exString;
    
    public Vector<Vector<Object>> vectorRec = new Vector();
    
    public GridTable(String setState, Dimension d) throws SQLException{
        recordDim=d;
           LLayer.LL.setStateFresh(setState);
           LLayer.LL.generateSimpleLook();
           LLayer.LL.sentquery();
           this.refresh();
    }
    
    public GridTable(String setState, int indicator, Dimension d) throws SQLException{
        recordDim=d;
        LLayer.LL.setStateFresh(setState);
        if(indicator == 1){
           LLayer.LL.callpass();
        }
        else if(indicator ==2){
           LLayer.LL.date_slot1 = "1900-1-1";
           LLayer.LL.date_slot2 = "2100-1-1";
           LLayer.LL.specialcase1sql();
        }
        else if(indicator ==3){
           LLayer.LL.date_slot1 = "1900-1-1";
           LLayer.LL.date_slot2 = "2100-1-1";
           LLayer.LL.specialcase2sql();
        }
        else{
           LLayer.LL.generateSimpleLookNoOrder();
        }
           LLayer.LL.sentquery();
           this.refresh();
        
    }
    
    public GridTable(Dimension d) throws SQLException{
        recordDim=d;
        this.addTable(d);
    }
    
    public void refresh() throws SQLException{
        this.removeAll();

        this.vectorRec.clear();
        this.addTable(recordDim);
    }

        private void addTable(Dimension d) throws SQLException{
            table = new JTable(buildTableModel(DBControllor.DBC.rs));
                 
            listSelectionModel = table.getSelectionModel();
            listSelectionModel.addListSelectionListener(new SharedListSelectionHandler());
            table.setSelectionModel(listSelectionModel);
                 
                 
            JScrollPane a = new JScrollPane(table);
            a.setPreferredSize(new Dimension(200,150));
            a.setPreferredSize(d);
            this.add(a);
        }
        
        
        
        public DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {
            ResultSetMetaData metaData = rs.getMetaData();
            // names of columns
            columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            this.columntypes = new int[columnCount+1];
            
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
                columntypes[column] = metaData.getColumnType(column);
                
            }

            // data of the table
            data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                 Vector<Object> vector2 = new Vector<Object>();
                
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    if(columntypes[columnIndex]==93){
                       String newS =(rs.getObject(columnIndex).toString());
                       newS = newS.substring(0, 10);
                       vector.add(newS);
                       vector2.add(newS);
                    }
                    else{
                    vector.add(rs.getObject(columnIndex));
                    vector2.add(rs.getObject(columnIndex));
                    }
                }
                data.add(vector);
                vectorRec.add(vector2);
            }
            
        return new DefaultTableModel(data, columnNames);
    }
    
        public void update() throws SQLException{
            for(int x =0 ; x<=table.getColumnCount()-1;x++){
                String newS =( vectorRec.get(table.getSelectedRow()).get(x).toString());
                LLayer.LL.slots1.add(newS);
            }
            for(int x =0 ; x<=table.getColumnCount()-1;x++){
                String newS = table.getValueAt(table.getSelectedRow(), x).toString();
                LLayer.LL.slots1.add(newS);
            }
            
            try {
                LLayer.LL.genrateQueryUpdate();
                LLayer.LL.sentquery();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage());  
                Logger.getLogger(GridTable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        public void delete() throws SQLException {
            for(int x =0 ; x<=table.getColumnCount()-1;x++){
                String newS =( vectorRec.get(table.getSelectedRow()).get(x).toString());
                LLayer.LL.slots1.add(newS);
            }
            try {
                LLayer.LL.genrateQueryDelete();
                LLayer.LL.sentquery();

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage());  
                Logger.getLogger(GridTable.class.getName()).log(Level.SEVERE, null, ex);
            }  
        }
        
        public void search(){
            int x =1;
            if(LLayer.LL.slots1.size()==(columntypes.length-1)){
                for(String EE: LLayer.LL.slots1){
                    SingleClause SC = new SingleClause();
                    SC.type = columntypes[x];
                    SC.attribute = columnNames.get(x-1);
                    SC.value1 = EE;
                    x++;
                    LLayer.LL.clause.add(SC);
                }
            }
            else{
                for(int y=0;;){
                    SingleClause SC = new SingleClause();
                    SC.type = columntypes[x];
                    if(SC.type==91||SC.type==93){
                        SC.attribute = columnNames.get(x-1);//columnNames is from "0"
                        SC.value1 = LLayer.LL.slots1.get(y);
                        SC.value2 = LLayer.LL.slots1.get(y+1);
                        System.out.println(SC.toString());
                        y=y+2;
                        x++;
                    }
                    else{
                        SC.attribute = columnNames.get(x-1);
                        SC.value1 = LLayer.LL.slots1.get(y);
                        y++;
                        x++;
                    }
                    LLayer.LL.clause.add(SC); 
                    if(y>(LLayer.LL.slots1.size()-1)){
                        break;
                    }
                }
            }
        try {
            LLayer.LL.genrateSearch();
             LLayer.LL.sentquery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,ex.getMessage());  
            Logger.getLogger(GridTable.class.getName()).log(Level.SEVERE, null, ex);
        }

     }
        
        
    class SharedListSelectionHandler implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) { 
//            vectorRec.clear();
//            
//            boolean isAdjusting = e.getValueIsAdjusting(); 
//            for(Object EE:data.get(table.getSelectedRow())){
//                String newS = (String)EE;
//            }
        }
    } 
}
