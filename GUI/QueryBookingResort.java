package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryBookingResort extends JPanel 
{
	private JTextField trip_id = new JTextField(5);
    private JTextField client_id = new JTextField(5);
    private JTextField resort_id = new JTextField(5);
    private JTextField room_type_id = new JTextField(2);
    private JTextField room_quantity = new JTextField(2);
    private JTextField arrival_date = new JTextField(6);
    private JTextField departure_date = new JTextField(6);
    private JTextField arrival_date_to = new JTextField(6);
    private JTextField departure_date_to = new JTextField(6);
    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh"),
    		updateButton = new JButton("Update"),
    		deleteButton = new JButton("Delete");
	
	public QueryBookingResort() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("bookingresortid", new Dimension(600,300));
        baseUpper.add(new JLabel("Trip ID:"));
        baseUpper.add(trip_id);
        baseUpper.add(new JLabel("Client ID:"));
        baseUpper.add(client_id);
        baseUpper.add(new JLabel("Resort ID:"));
        baseUpper.add(resort_id);
        baseUpper.add(new JLabel("Room Type ID:"));
        baseUpper.add(room_type_id);
        baseUpper.add(new JLabel("Room Quantity:"));
        baseUpper.add(room_quantity);
        
        baseUpper2.add(new JLabel("Arrival Time From:"));
        baseUpper2.add(arrival_date);
        baseUpper2.add(new JLabel("Arrival Time To:"));
        baseUpper2.add(arrival_date_to);
        baseUpper2.add(new JLabel("Departure Time From:"));
        baseUpper2.add(departure_date);
        baseUpper2.add(new JLabel("Departure Time To:"));
        baseUpper2.add(departure_date_to);
        
        baseUpper3.add(new JLabel("Date Format: yyyy-mm-dd            "));
        baseUpper3.add(searchButton);
        up.add(baseUpper);
        up.add(baseUpper2);
        up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
        
        updateButton.addActionListener(new Update());
        deleteButton.addActionListener(new Delete());
        JPanel bot = new JPanel(new FlowLayout());
        bot.add(updateButton);
        bot.add(deleteButton);
        base.add(bot, BorderLayout.SOUTH);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                 LLayer.LL.setStateFresh("bookingresortid");
                    LLayer.LL.slots1.add(trip_id.getText());
                    LLayer.LL.slots1.add(client_id .getText());
                    LLayer.LL.slots1.add(resort_id.getText());
                    LLayer.LL.slots1.add(room_type_id .getText());
                    LLayer.LL.slots1.add(room_quantity.getText());
                    LLayer.LL.slots1.add(arrival_date.getText());
                    LLayer.LL.slots1.add(arrival_date_to.getText());
                    LLayer.LL.slots1.add(departure_date.getText());
                    LLayer.LL.slots1.add(departure_date_to.getText());
                    try {
                        table.search();
                        refresh();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    }    
                    
        }
    }	
    
	private class Update extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                LLayer.LL.setStateFresh("bookingresortid");
                try {
                     table.update();
                } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(QueryBookingResort.this,"You have successfully updated the information! " +
        				"You can press Search/Refersh to see the current result.");
        }
	}
         
    private class Delete extends AbstractAction{
	@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("bookingresortid");
            try {
            table.delete();
            } catch (SQLException ex) {
             Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(QueryBookingResort.this,"You have successfully deleted the information! " +
    				"You can press Search/Refersh to see the current result.");
        }
	}
	
}