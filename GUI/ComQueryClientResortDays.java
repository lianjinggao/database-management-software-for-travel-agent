package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryClientResortDays extends JPanel 
{
	private JTextField resort = new JTextField(10);
    private JTextField first = new JTextField(5);
    private JTextField last = new JTextField(5);
    private JTextField days = new JTextField(5);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
	
	public ComQueryClientResortDays() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("clientresortdays",0, new Dimension(500,300));
        baseUpper.add(new JLabel("Resort Name:"));
        baseUpper.add(resort);
        baseUpper.add(new JLabel("First Name:"));
        baseUpper.add(first);
        baseUpper.add(new JLabel("Last Name:"));
        baseUpper.add(last);
        baseUpper.add(new JLabel("Days Stayed:"));
        baseUpper.add(days);

        baseUpper3.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("clientresortdays");
            //LLayer.LL.callView("totalrevenuecountry");
            
            LLayer.LL.slots1.add(resort.getText());
            LLayer.LL.slots1.add(first.getText());
            LLayer.LL.slots1.add(last.getText());
            LLayer.LL.slots1.add(days.getText());
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }	
}