package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryClientTrip extends JPanel 
{
    private JTextField cilentFirstName = new JTextField(5);
    private JTextField cilentLastName = new JTextField(5);
    private JTextField agentFirstName = new JTextField(5);
    private JTextField agentLastName = new JTextField(5);
    private JTextField resortName = new JTextField(10);
    private JTextField roomType = new JTextField(5);
    private JTextField roomQuantity = new JTextField(2);
    
    private JTextField bookingDate = new JTextField(6);
    private JTextField bookingDateTo = new JTextField(6);
    private JTextField arrivalDate = new JTextField(6);
    private JTextField arrivalDateTo = new JTextField(6);
    private JTextField departureDate = new JTextField(6);
    private JTextField departureDateTo = new JTextField(6);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
	
	public ComQueryClientTrip() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper1_5 = new JPanel(new FlowLayout());
	    JPanel baseUpper2 = new JPanel(new FlowLayout());
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    JPanel baseUpper4 = new JPanel(new FlowLayout()); 
	    JPanel baseUpper5 = new JPanel(new FlowLayout()); 

	    
        searchButton.addActionListener(new Search());
        table = new GridTable("clienttrip",0, new Dimension(700,300));

        baseUpper.add(new JLabel("Client First Name:"));
        baseUpper.add(cilentFirstName);
        baseUpper.add(new JLabel("Client Last Name:"));
        baseUpper.add(cilentLastName);
        baseUpper.add(new JLabel("Agent First Name:"));
        baseUpper.add(agentFirstName);
        baseUpper.add(new JLabel("Agent Last Name:"));
        baseUpper.add(agentLastName);
        

        baseUpper1_5.add(new JLabel("Resort Name"));
        baseUpper1_5.add(resortName);
        baseUpper1_5.add(new JLabel("Room Type"));
        baseUpper1_5.add(roomType);
        baseUpper1_5.add(new JLabel("Room Quantity"));
        baseUpper1_5.add(roomQuantity);
        
        baseUpper2.add(new JLabel("Booking Date From:"));
        baseUpper2.add(bookingDate);
        baseUpper2.add(new JLabel("Booking Date To:"));
        baseUpper2.add(bookingDateTo);
        
        baseUpper3.add(new JLabel("Arrival Date From:"));
        baseUpper3.add(arrivalDate);
        baseUpper3.add(new JLabel("Arrival Date To:"));
        baseUpper3.add(arrivalDateTo);
        
        baseUpper4.add(new JLabel("Departure Date From:"));
        baseUpper4.add(departureDate);
        baseUpper4.add(new JLabel("Departure Date To:"));
        baseUpper4.add(departureDateTo);
        
        baseUpper5.add(new JLabel("Date Format: yyyy-mm-dd            "));
        baseUpper5.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper1_5);
        up.add(baseUpper2);
        up.add(baseUpper3);
        up.add(baseUpper4);
        up.add(baseUpper5);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			
            LLayer.LL.setStateFresh("clienttrip");
            LLayer.LL.slots1.add(cilentFirstName.getText());
            LLayer.LL.slots1.add(cilentLastName.getText());
            LLayer.LL.slots1.add(agentFirstName.getText());
            LLayer.LL.slots1.add(agentLastName.getText());
            LLayer.LL.slots1.add(bookingDate.getText());
            LLayer.LL.slots1.add(bookingDateTo.getText());
            LLayer.LL.slots1.add(resortName.getText());
            LLayer.LL.slots1.add(roomType.getText());
            LLayer.LL.slots1.add(roomQuantity.getText());
            LLayer.LL.slots1.add(arrivalDate.getText());
            LLayer.LL.slots1.add(arrivalDateTo.getText());
            LLayer.LL.slots1.add(departureDate.getText());
            LLayer.LL.slots1.add(departureDateTo.getText());
            
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }	
}