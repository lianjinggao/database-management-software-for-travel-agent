package GUI;
import dbapp.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;


class Login extends JFrame{
	
    private  JTextField userID;
    private  JPasswordField userPass;
    Main mainGUI;
	public Login(){
		
        LLayer.LL.getConnect();
            
		this.setTitle("Login");
		this.setSize(220, 200);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
        JPanel base = new JPanel();
	    base.setLayout(new BorderLayout());
	    
	    //top
	    JPanel top = new JPanel();
	    top.setLayout(new FlowLayout());
	    JLabel info = new JLabel("Login");
	    top.add(info);
	    base.add(top, BorderLayout.PAGE_START);

	    //mid
	    JPanel mid = new JPanel();
	    mid.setLayout(new GridLayout(0,2));
	    
	    JLabel userN = new JLabel("User ID:");
	    userID = new JTextField();
	    JLabel userP = new JLabel("Password:");
	    userPass= new JPasswordField();
	    mid.add(userN);
	    mid.add(userID);
	    mid.add(userP);
	    mid.add(userPass);
	    base.add(mid, BorderLayout.CENTER);
	    
	    //bot
	    JPanel bot = new JPanel();
	    bot.setLayout(new BoxLayout(bot,BoxLayout.Y_AXIS));
	    JPanel firstR = new JPanel(new FlowLayout());
	    JPanel secondR = new JPanel(new FlowLayout());
	    JButton register = new JButton("Sign up");
	    JButton login = new JButton("login");
	    JButton exit = new JButton("exit");
	    register.addActionListener(new RegisterAction());
	    exit.addActionListener(new ExitAction());
	    login.addActionListener(new LoginAction());
	    firstR.add(login);
	    firstR.add(exit);
	    secondR.add(register);
	    bot.add(firstR);
	    bot.add(secondR);
	    base.add(bot, BorderLayout.PAGE_END);
	    
	    this.add(base);
	    //this.pack();
	    this.setLocationRelativeTo(null);
	    this.setVisible(true);
	}
	
	private class RegisterAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			SignUp registerPage = new SignUp();
			registerPage.setVisible(true);
		}
		
	}
	
	private class ExitAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
		 
	}
	
	private class LoginAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
            try {
            	if(userID.getText() == null || userID.getText().isEmpty()
            			||userPass.getPassword() == null || userPass.getPassword().length == 0){
            		JOptionPane.showMessageDialog(Login.this,"ID/password cannot be empty!");
            		Login loginGUI = new Login();
        		}
            	else if(LLayer.LL.verifyLogin(userID.getText(),userPass.getPassword()) == true)
            		mainGUI = new Main();
            	else {
            		JOptionPane.showMessageDialog(Login.this,"Wrong id/password!");
            		Login loginGUI = new Login();
            	}
            } catch (SQLException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
            dispose();
		}
		 
	}
}