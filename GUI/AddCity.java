package GUI;
import javax.swing.*;

import dbapp.LLayer;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddCity extends JPanel{
	
	private String city;
	private String country;
	
	private JTextField cityText = new JTextField(10);
	private JTextField countryText = new JTextField(10);
	private JPanel base = new JPanel(new BorderLayout());
	
	public AddCity(){
		
		//left
		JPanel left = new JPanel(new GridLayout(0,1));
		left.add(new JLabel("City:"));
		left.add(new JLabel("Country:"));
		base.add(left, BorderLayout.LINE_START);
		
		//right
		JPanel right = new JPanel(new GridLayout(0,1));
		
		right.add(cityText);
		right.add(countryText);
		base.add(right, BorderLayout.LINE_END);
		
		//bot
		//JPanel bot
		JPanel bot = new JPanel(new FlowLayout());
		JButton add = new JButton("Add");
		add.addActionListener(new addAction());
		bot.add(add);
		
		base.add(bot, BorderLayout.PAGE_END);
		
		this.add(base);
	}
	
	private class addAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			LLayer.LL.setStateFresh("addcity");
			LLayer.LL.name_slot1=cityText.getText();
            LLayer.LL.name_slot2=countryText.getText(); 

                boolean flag = true;
	         try {
                    if(LLayer.LL.checkInfo()==false){
                        JOptionPane.showMessageDialog(AddCity.this,"Names can only contain letters.");
                        flag = false;
                        return;
                    }
	            LLayer.LL.genrateQuery();
	            LLayer.LL.sentquery();             
                    
                    cityText.setText("");
                    countryText.setText("");
	         } catch (SQLException ex) {
	             Logger.getLogger(AddClient.class.getName()).log(Level.SEVERE, null, ex);
	         }
                 if(flag&&LLayer.LL.exflag){
                    JOptionPane.showMessageDialog(AddCity.this,"You have successfully added the city information!");
                 }
		}
	}
}
