package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryResort extends JPanel 
{
	private JTextField id = new JTextField(10);
    private JTextField name = new JTextField(10);
    private JTextField city = new JTextField(10);
    private JTextField address = new JTextField(15);
    private JTextField phone = new JTextField(10);
    private JTextField rating = new JTextField(1);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh"),
    		updateButton = new JButton("Update"),
    		deleteButton = new JButton("Delete");
	
	public QueryResort() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("queryresort",new Dimension(700,300));
        baseUpper.add(new JLabel("ID:"));
        baseUpper.add(id);
        baseUpper.add(new JLabel("Name:"));
        baseUpper.add(name);
        baseUpper.add(new JLabel("City:"));
        baseUpper.add(city);
        
        baseUpper2.add(new JLabel("Address:"));
        baseUpper2.add(address);
        baseUpper2.add(new JLabel("Phone:"));
        baseUpper2.add(phone);
        baseUpper2.add(new JLabel("Sun Rating:"));
        baseUpper2.add(rating);
        baseUpper2.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper2);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
        
        updateButton.addActionListener(new Update());
        deleteButton.addActionListener(new Delete());
        JPanel bot = new JPanel(new FlowLayout());
      if(LLayer.LL.getpriv()==true){
            bot.add(updateButton);
            bot.add(deleteButton);
        }
        base.add(bot, BorderLayout.SOUTH);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
    	//table.a.setPreferredSize(new Dimension(600,300));
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("queryresort");
            LLayer.LL.slots1.add(id.getText());
            LLayer.LL.slots1.add(name.getText());
             LLayer.LL.slots1.add(city.getText());
              LLayer.LL.slots1.add(address.getText());
              LLayer.LL.slots1.add(phone.getText());
               LLayer.LL.slots1.add(rating.getText());
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            }    
        }
    }	
    
	private class Update extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                LLayer.LL.setStateFresh("queryresort");
                try {
                     table.update();
                } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(QueryResort.this,"You have successfully updated the information! " +
        				"You can press Search/Refersh to see the current result.");
        }
	}
         
    private class Delete extends AbstractAction{
	@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("queryroomtype");
            try {
            table.delete();
            } catch (SQLException ex) {
             Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(QueryResort.this,"You have successfully deleted the information! " +
    				"You can press Search/Refersh to see the current result.");
        }
	}
	
}