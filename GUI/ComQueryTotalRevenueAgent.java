package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryTotalRevenueAgent extends JPanel 
{
	private JTextField total = new JTextField(5);
    private JTextField agentFirstName = new JTextField(5);
    private JTextField agentLastName = new JTextField(5);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
	
	public ComQueryTotalRevenueAgent() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("totalrevenueagent",0, new Dimension(200,300));
        baseUpper.add(new JLabel("Total Revenue:"));
        baseUpper.add(total);
        baseUpper.add(new JLabel("Agent First Name:"));
        baseUpper.add(agentFirstName);
        baseUpper.add(new JLabel("Agent Last Name:"));
        baseUpper.add(agentLastName);

        baseUpper3.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("totalrevenueagent");
            
            LLayer.LL.slots1.add(total.getText());
            LLayer.LL.slots1.add(agentFirstName.getText());
            LLayer.LL.slots1.add(agentLastName.getText());
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }	
}