package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryBookingID extends JPanel 
{
	private JTextField tripid = new JTextField(10);
    private JTextField personid = new JTextField(10);
    private JTextField agentid = new JTextField(10);
    private JTextField bookingYear = new JTextField(4);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh"),
    		updateButton = new JButton("Update"),
    		deleteButton = new JButton("Delete");
	
	public QueryBookingID() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("trip_payment", new Dimension(500,300));
        baseUpper.add(new JLabel("Trip ID:"));
        baseUpper.add(tripid);
        baseUpper.add(new JLabel("Client ID:"));
        baseUpper.add(personid);
        baseUpper.add(new JLabel("Agent ID:"));
        baseUpper.add(agentid);
        
        baseUpper2.add(new JLabel("Booking date:"));
        baseUpper2.add(bookingYear);

        baseUpper2.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper2);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
        
        updateButton.addActionListener(new Update());
        deleteButton.addActionListener(new Delete());
        JPanel bot = new JPanel(new FlowLayout());
        bot.add(updateButton);
        bot.add(deleteButton);
        base.add(bot, BorderLayout.SOUTH);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                    LLayer.LL.setStateFresh("trip_payment");
                    LLayer.LL.slots1.add(tripid.getText());
                    LLayer.LL.slots1.add(personid .getText());
                    LLayer.LL.slots1.add(agentid.getText());
                    LLayer.LL.slots1.add(bookingYear.getText());
                    try {
                        table.search();
                        refresh();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    } 

        }
    }	
    
	private class Update extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                LLayer.LL.setStateFresh("trip_payment");
                try {
                     table.update();
                } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(QueryBookingID.this,"You have successfully updated the information! " +
        				"You can press Search/Refersh to see the current result.");
        }
	}
         
    private class Delete extends AbstractAction{
	@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("trip_payment");
            try {
            table.delete();
            } catch (SQLException ex) {
             Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(QueryBookingID.this,"You have successfully deleted the information! " +
    				"You can press Search/Refersh to see the current result.");
        }
	}
	
}