package GUI;


import dbapp.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Main extends JFrame{
	
	private JPanel cardDefault = new JPanel();
	
	private AddClient addClient = new AddClient();
	private AddCity addCity = new AddCity();
	private AddAmenity addAmenity = new AddAmenity();
	
	private AddResort addResort = new AddResort();
	private AddRoomType addRoomType = new AddRoomType();
	private AddBooking addBooking = new AddBooking();
	private AddRoom addRoom = new AddRoom();

	private QueryCity queryCity = new QueryCity(); 
	
	JPanel mid = new JPanel();
	
	public Main() throws SQLException{
		
		this.setTitle("Main Interface");
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setSize(700,310);
		
		JPanel base = new JPanel();
		base.setLayout(new BorderLayout());
				
		//top
		JPanel top = new JPanel();
		top.setLayout(new BoxLayout(top, BoxLayout.LINE_AXIS));
		JMenuBar menuBar= new JMenuBar();
		JMenu menuAdd = new JMenu("Add");
		JMenu menuQuery = new JMenu("Query");
		menuAdd.setMnemonic(KeyEvent.VK_A);
		menuQuery.setMnemonic(KeyEvent.VK_Q);
		//JMenu menuDelete = new JMenu("Delete");
		
		JMenuItem menuItemAddClient = new JMenuItem("Client", KeyEvent.VK_C);
		JMenuItem menuItemAddCity = new JMenuItem("City", KeyEvent.VK_I);
		JMenuItem menuItemAddAmenity = new JMenuItem("Amenity", KeyEvent.VK_A);
		JMenuItem menuItemAddResort = new JMenuItem("Resort", KeyEvent.VK_R);
		JMenuItem menuItemAddRoomType = new JMenuItem("Room Type", KeyEvent.VK_R);
		JMenuItem menuItemAddBooking = new JMenuItem("Booking", KeyEvent.VK_B);
		JMenuItem menuItemAddRoom = new JMenuItem("Room", KeyEvent.VK_O);
		JMenuItem close = new JMenuItem("Exit", KeyEvent.VK_O);
		
		menuItemAddClient.addActionListener(new ShowAddClient());
		menuItemAddCity.addActionListener(new ShowAddCity());
		menuItemAddAmenity.addActionListener(new ShowAddAmenity());
		menuItemAddResort.addActionListener(new ShowAddResort());
		menuItemAddRoomType.addActionListener(new ShowAddRoomType());
		menuItemAddBooking.addActionListener(new ShowAddBooking());
		menuItemAddRoom.addActionListener(new ShowAddRoom());
		close.addActionListener(new ExitAction());
		
		menuAdd.add(menuItemAddClient);
		menuAdd.add(menuItemAddBooking);
		menuAdd.addSeparator();
                
                if(LLayer.LL.getpriv()==true){
                    menuAdd.add(menuItemAddCity);
                    menuAdd.add(menuItemAddResort);
                    menuAdd.add(menuItemAddAmenity);
                    menuAdd.add(menuItemAddRoomType);
                    menuAdd.add(menuItemAddRoom);
                    menuAdd.addSeparator();
                }
		
		menuAdd.add(close);
		
		//query
		JMenuItem menuItemQuery = new JMenuItem("Query", KeyEvent.VK_Q);
		menuItemQuery.addActionListener(new ShowQuery());
		menuQuery.add(menuItemQuery);
		
		//delete
		//JMenuItem menuItemDeleteClient = new JMenuItem("Add Client", KeyEvent.VK_L);
//		JMenuItem menuItemDeleteCity = new JMenuItem("City", KeyEvent.VK_C);
//		JMenuItem menuItemDeleteAmenity = new JMenuItem("Amenity", KeyEvent.VK_A);
//		menuDelete.add(menuItemDeleteCity);
//		menuDelete.add(menuItemDeleteAmenity);
		
		menuBar.add(menuAdd);
		menuBar.add(menuQuery);
		//menuBar.add(menuDelete);
		top.add(menuBar);
		base.add(top, BorderLayout.PAGE_START);
		
		
		//mid
		mid.setLayout(new CardLayout());
		
		cardDefault.add(new JLabel("Welcome! "));
		cardDefault.add(new JLabel("Please select operations on the menu. "));
		cardDefault.add(new JLabel("Thank you! "));
		cardDefault.add(new JLabel("Note: Case Sensitive!"));
		
		mid.add(cardDefault, "AddDefault");
		mid.add(addClient, "AddClient");
		mid.add(addBooking, "AddBooking");
		mid.add(addCity, "AddCity");
		mid.add(addResort, "AddResort");
		mid.add(addAmenity, "AddAmenity");
		mid.add(addRoomType, "AddRoomType");
		mid.add(addRoom, "AddRoom");
		mid.add(queryCity,"QueryCity");
		
		base.add(mid, BorderLayout.CENTER);
		
		//bot
		JPanel bot = new JPanel();
		bot.setLayout(new FlowLayout());
		JButton logout = new JButton("Logout");
		JButton exit = new JButton("Exit");
		exit.addActionListener(new ExitAction());
		logout.addActionListener(new LogoutAction());
		//bot.add(logout);
		bot.add(exit);
		base.add(bot, BorderLayout.PAGE_END);
		
		
		this.add(base);
		//this.pack();
	    this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private class ShowAddClient extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "AddClient");
			Main.this.setTitle("Add Client");
		}
	}
	
	private class ShowAddCity extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "AddCity");
			Main.this.setTitle("Add City");
		}
	}
	
	private class ShowAddAmenity extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "AddAmenity");
			Main.this.setTitle("Add Amenity");
		}
	}
	
	private class ShowAddResort extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "AddResort");
			Main.this.setTitle("Add Resort");
		}
	}
	
	private class ShowAddRoomType extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "AddRoomType");
			Main.this.setTitle("Add Room Type");
		}
	}
	
	private class ShowAddBooking extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "AddBooking");
			Main.this.setTitle("Add Booking");
		}
	}
	
	private class ShowAddRoom extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "AddRoom");
			Main.this.setTitle("Add Room");
		}
	}
	
	private class ShowQuery extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				QueryGUI windows = new QueryGUI();
			} catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
		}
	}
	
	private class ExitAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
            try {
                DBControllor.DBC.close();
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
		}
	}
	
	private class LogoutAction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			dispose();
			Login loginGUI = new Login();
		}
	}

}
