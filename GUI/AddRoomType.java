package GUI;

import dbapp.LLayer;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddRoomType extends JPanel
{
	private JTextField text;
	public AddRoomType(){
		JPanel base = new JPanel(new BorderLayout());
		text = new JTextField(10);
		JButton add = new JButton("Add");
		add.addActionListener(new AddAction());
		JPanel row1 = new JPanel(new FlowLayout());
		JPanel row2 = new JPanel(new FlowLayout());
		row1.add(new JLabel("Room_type_name:"));
		row1.add(text);
		row2.add(add);
		base.add(row1,BorderLayout.CENTER);
		base.add(row2,BorderLayout.PAGE_END);
		this.add(base);
	}
	
	private class AddAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			LLayer.LL.setStateFresh("addroomtype");
    		LLayer.LL.name_slot1 = text.getText();
    		try {
                LLayer.LL.genrateQuery();
                LLayer.LL.sentquery();
                JOptionPane.showMessageDialog(AddRoomType.this,"You have successfully added the room type information!");
    		text.setText("");
            } catch (SQLException ex) {
                Logger.getLogger(AddRoomType.class.getName()).log(Level.SEVERE, null, ex);
            }			

	}
		
	}
}
