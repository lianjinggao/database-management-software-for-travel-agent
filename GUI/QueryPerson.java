package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryPerson extends JPanel 
{
	private JTextField id = new JTextField(10);
    private JTextField firstName = new JTextField(10);
    private JTextField lastName = new JTextField(10);
    private JTextField dob = new JTextField(4);

    private JTextField gender = new JTextField(1);
    private JTextField zipcode = new JTextField(5);
    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh"),
    		updateButton = new JButton("Update"),
    		deleteButton = new JButton("Delete");
	
	public QueryPerson() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("queryperson", new Dimension(500,300));
        baseUpper.add(new JLabel("ID:"));
        baseUpper.add(id);
        baseUpper.add(new JLabel("First Name:"));
        baseUpper.add(firstName);
        baseUpper.add(new JLabel("Last Name:"));
        baseUpper.add(lastName);
        
        baseUpper2.add(new JLabel("date of birth:"));
        baseUpper2.add(dob);
        baseUpper2.add(new JLabel("Gender:"));
        baseUpper2.add(gender);
        baseUpper2.add(new JLabel("Zipcode:"));
        baseUpper2.add(zipcode);
        baseUpper2.add(searchButton);
        up.add(baseUpper);
        up.add(baseUpper2);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
        
        updateButton.addActionListener(new Update());
        deleteButton.addActionListener(new Delete());
        JPanel bot = new JPanel(new FlowLayout());
        bot.add(updateButton);
        bot.add(deleteButton);
        base.add(bot, BorderLayout.SOUTH);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                 LLayer.LL.setStateFresh("queryclient");
                    LLayer.LL.slots1.add(id.getText());
                    LLayer.LL.slots1.add(firstName .getText());
                    LLayer.LL.slots1.add(lastName.getText());
                    LLayer.LL.slots1.add(dob.getText());
                    LLayer.LL.slots1.add(gender.getText());
                    LLayer.LL.slots1.add(zipcode.getText());
                    try {
                        table.search();
                        refresh();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    }    
        }
    }	
    
	private class Update extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                LLayer.LL.setStateFresh("addclient");
                try {
                     table.update();
                } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(QueryPerson.this,"You have successfully updated the information! " +
        				"You can press Search/Refersh to see the current result.");
        }
	}
         
    private class Delete extends AbstractAction{
	@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("addclient");
            try {
            table.delete();
            } catch (SQLException ex) {
             Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(QueryPerson.this,"You have successfully deleted the information! " +
    				"You can press Search/Refersh to see the current result.");
        }
	
	}
	
}