package GUI;

import dbapp.LLayer;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
	
public class AddBooking extends JPanel{
	//private int personID, agentID, resortID, hotelID, roomQuantity;
	//private String bookingDate, arrivialDate, departureDate, roomType;
	private JTextField personText,
						 agentText,						 
						 bookingYearText,bookingMonthText,bookingDayText,
						 arrivalYearText,arrivalMonthText,arrivalDayText,
						 departureYearText,departureMonthText,departureDayText,
						 resortText,
						 roomText,
						 tripText;
	private JComboBox roomQuantityBox;
	private JCheckBox tripBox;
	private int quantity = 1;
	
	public AddBooking(){
		JPanel base = new JPanel(new BorderLayout());
		
		//left
		JPanel left = new JPanel();
		left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
		
		JPanel firstRow = new JPanel(new FlowLayout());
		JPanel secondRow = new JPanel(new FlowLayout());
		JPanel thirdRow = new JPanel(new FlowLayout());
		JPanel fourthRow = new JPanel(new FlowLayout());
		JPanel fifthRow = new JPanel(new FlowLayout());
		
		JLabel personLabel = new JLabel("Person_id:");
		personLabel.setToolTipText("This is the person who pays for the trip.");
		JLabel agentLabel = new JLabel("Agent_id:");
		JLabel bookingLabel = new JLabel("Booking_date:   ");
		JLabel bookingYearLabel = new JLabel("Year:");
		JLabel bookingMonthLabel = new JLabel("Month:");
		JLabel bookingDayLabel = new JLabel("Day:");
		JLabel arrivalLabel = new JLabel("Arrival_date:     ");
		JLabel arrivalYearLabel = new JLabel("Year:");
		JLabel arrivalMonthLabel = new JLabel("Month:");
		JLabel arrivalDayLabel = new JLabel("Day:");
		JLabel departureLabel = new JLabel("Departure_date:");
		JLabel departureYearLabel = new JLabel("Year:");
		JLabel departureMonthLabel = new JLabel("Month:");
		JLabel departureDayLabel = new JLabel("Day:");
		
		personText = new JTextField(10);
		agentText = new JTextField(10);
		bookingYearText = new JTextField(4);
		bookingMonthText = new JTextField(2);
		bookingDayText = new JTextField(2);
		arrivalYearText = new JTextField(4);
		arrivalMonthText = new JTextField(2);
		arrivalDayText = new JTextField(2);
		departureYearText = new JTextField(4);
		departureMonthText = new JTextField(2);
		departureDayText = new JTextField(2);
			
		firstRow.add(personLabel);
		firstRow.add(personText);
		
		secondRow.add(agentLabel);
		secondRow.add(agentText);;
		
		thirdRow.add(bookingLabel);
		thirdRow.add(bookingYearLabel);
		thirdRow.add(bookingYearText);
		thirdRow.add(bookingMonthLabel);
		thirdRow.add(bookingMonthText);
		thirdRow.add(bookingDayLabel);
		thirdRow.add(bookingDayText);
		
		fourthRow.add(arrivalLabel);
		fourthRow.add(arrivalYearLabel);
		fourthRow.add(arrivalYearText);
		fourthRow.add(arrivalMonthLabel);
		fourthRow.add(arrivalMonthText);
		fourthRow.add(arrivalDayLabel);
		fourthRow.add(arrivalDayText);
		
		fifthRow.add(departureLabel);
		fifthRow.add(departureYearLabel);
		fifthRow.add(departureYearText);
		fifthRow.add(departureMonthLabel);
		fifthRow.add(departureMonthText);
		fifthRow.add(departureDayLabel);
		fifthRow.add(departureDayText);
		
		left.add(firstRow);
		left.add(secondRow);
		left.add(thirdRow);
		left.add(fourthRow);
		left.add(fifthRow);
		
		base.add(left ,BorderLayout.LINE_START);
		
		
		//right
		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.PAGE_AXIS));
		
		JPanel firstRowR = new JPanel(new FlowLayout());
		JPanel secondRowR = new JPanel(new FlowLayout());
		JPanel thirdRowR = new JPanel(new FlowLayout());
		JPanel fourthRowR = new JPanel(new FlowLayout());
		JPanel newRow = new JPanel(new FlowLayout());
		
		JLabel resortLabel = new JLabel("Resort_id:");
		JLabel hotelLabel = new JLabel("Room_type_id:");
		JLabel roomQuantityLabel = new JLabel("Room_quantity:");
		
		resortText = new JTextField(10);
		roomText = new JTextField(5);
		
		String [] quantity = {"1","2","3","4"};
		
		roomQuantityBox = new JComboBox(quantity);
		roomQuantityBox.addActionListener(new roomQuantityAction());
		JLabel tripLabel = new JLabel("Trip_id:");
		tripBox = new JCheckBox();
		tripText = new JTextField(10);
		tripText.setEditable(false);
		tripBox.addActionListener(new tripBoxAction());
		
		firstRowR.add(resortLabel);
		firstRowR.add(resortText);
		
		secondRowR.add(hotelLabel);
		secondRowR.add(roomText);
		
		thirdRowR.add(roomQuantityLabel);
		thirdRowR.add(roomQuantityBox);
		
		newRow.add(new JLabel("Assign a new client with same trip_id here:"));
		
		fourthRowR.add(tripLabel);
		fourthRowR.add(tripBox);
		fourthRowR.add(tripText);
		
		right.add(firstRowR);
		right.add(secondRowR);
		right.add(thirdRowR);
		right.add(newRow);
		right.add(fourthRowR);

		
		base.add(right, BorderLayout.LINE_END);

		//bot
		JPanel bot = new JPanel(new FlowLayout());
		JButton submit = new JButton("Submit");
                submit.addActionListener(new AddAction());
		bot.add(submit);

		base.add(bot, BorderLayout.PAGE_END);
		
		this.add(base);
	}
	private class AddAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			LLayer.LL.setStateFresh("addbooking");
			LLayer.LL.id_slot1 = personText.getText();
			LLayer.LL.id_slot2 = agentText.getText();
			LLayer.LL.date_slot1 = bookingMonthText.getText()+"-"+bookingDayText.getText()+"-"+bookingYearText.getText();
			LLayer.LL.date_slot2 = arrivalMonthText.getText()+"-"+arrivalDayText.getText()+"-"+arrivalYearText.getText();
			LLayer.LL.date_slot3 = departureMonthText.getText()+"-"+departureDayText.getText()+"-"+departureYearText.getText();
			LLayer.LL.id_slot3 = resortText.getText();
			LLayer.LL.id_slot4 = roomText.getText();
			LLayer.LL.rating_slot = String.valueOf(quantity);
			if(tripBox.isSelected())
				LLayer.LL.id_slot5 = tripText.getText();
         try {
             LLayer.LL.genrateQuery();
             LLayer.LL.sentquery();
              JOptionPane.showMessageDialog(AddBooking.this,"You have successfully added the booking information!");
         personText.setText("");
         agentText.setText("");
 		resortText.setText("");
 		bookingMonthText.setText("");
 		bookingDayText.setText("");
 		bookingYearText.setText("");
 		arrivalMonthText.setText("");
 		arrivalDayText.setText("");
 		arrivalYearText.setText("");
 		departureMonthText.setText("");
 		departureDayText.setText("");
 		departureYearText.setText("");
 		resortText.setText("");
 		roomText.setText("");
 		roomQuantityBox.setSelectedIndex(0);
         } catch (SQLException ex) {
             Logger.getLogger(AddClient.class.getName()).log(Level.SEVERE, null, ex);
         }
        
		}
	}
	
	private class tripBoxAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(((JCheckBox)e.getSource()).isSelected())
				tripText.setEditable(true);
			else{
				tripText.setEditable(false);
				tripText.setText("");
			}
		}
		
	}
	
	private class roomQuantityAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox)e.getSource();
	        quantity = Integer.valueOf((String)cb.getSelectedItem());
	        System.out.println(quantity);			
		}
		
	}
}
