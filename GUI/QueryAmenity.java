package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryAmenity extends JPanel 
{
	private JTextField id = new JTextField(5);
    private JTextField name = new JTextField(15);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh"),
    		updateButton = new JButton("Update"),
    		deleteButton = new JButton("Delete");
	
	public QueryAmenity() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("queryamenity",GridTable.DEFAULT_DIMENSION);
        baseUpper.add(new JLabel("ID:"));
        baseUpper.add(id);
        baseUpper.add(new JLabel("Amenity Name:"));
        baseUpper.add(name);
        baseUpper.add(searchButton);
        
        base.add(baseUpper, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
        
        updateButton.addActionListener(new Update());
        deleteButton.addActionListener(new Delete());
        JPanel bot = new JPanel(new FlowLayout());
        if(LLayer.LL.getpriv()==true){
            bot.add(updateButton);
            bot.add(deleteButton);
        }
        base.add(bot, BorderLayout.SOUTH);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("queryamenity");
                    LLayer.LL.slots1.add(id.getText());
                    LLayer.LL.slots1.add(name .getText());
                    try {
                        table.search();
                        refresh();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    }  
        }
    }	
    
	private class Update extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                LLayer.LL.setStateFresh("queryamenity");
                try {
                     table.update();
                } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(QueryAmenity.this,"You have successfully updated the information! " +
        				"You can press Search/Refersh to see the current result.");
        }
	}
         
    private class Delete extends AbstractAction{
	@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("queryamenity");
            try {
            table.delete();
            } catch (SQLException ex) {
             Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(QueryAmenity.this,"You have successfully deleted the information! " +
    				"You can press Search/Refersh to see the current result.");
        }
	}
	
}