package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryAvgDaysPerTrip extends JPanel 
{
	private JTextField days = new JTextField(5);
    private JTextField first = new JTextField(5);
    private JTextField last = new JTextField(5);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
	
	public ComQueryAvgDaysPerTrip() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    //JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("avgdayspertrip",0, new Dimension(300,300));
        
        baseUpper.add(new JLabel("Average Days Per Trip:"));
        baseUpper.add(days);
        baseUpper.add(new JLabel("First Name:"));
        baseUpper.add(first);
        baseUpper.add(new JLabel("Last Name:"));
        baseUpper.add(last);
        

        baseUpper.add(searchButton);
        
        up.add(baseUpper);
        //up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("avgdayspertrip");
            //LLayer.LL.callView("totalrevenuecountry");
            
            LLayer.LL.slots1.add(days.getText());
            LLayer.LL.slots1.add(first.getText());
            LLayer.LL.slots1.add(last.getText());
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }	
}