package GUI;

import dbapp.LLayer;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddRoom extends JPanel{
	private JTextField resortText, roomText, priceText;
	public AddRoom(){
		JPanel base = new JPanel(new BorderLayout());
		resortText = new JTextField(10);
		roomText = new JTextField(5);
		priceText = new JTextField(5);
		JButton add = new JButton("Add");
		add.addActionListener(new AddAction());
		JPanel row1 = new JPanel(new FlowLayout());
		JPanel row2 = new JPanel(new FlowLayout());
		row1.add(new JLabel("Resort id:"));
		row1.add(resortText);
		row1.add(new JLabel("Room id:"));
		row1.add(roomText);
		row1.add(new JLabel("Price:"));
		row1.add(priceText);
		row2.add(add);
		base.add(row1,BorderLayout.CENTER);
		base.add(row2,BorderLayout.PAGE_END);
		this.add(base);
	}
	
	private class AddAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			LLayer.LL.setStateFresh("addroom");
    		LLayer.LL.id_slot1 = resortText.getText();
    		LLayer.LL.id_slot2 = roomText.getText();
    		LLayer.LL.price_slot1 = priceText.getText();
    		try {
                LLayer.LL.genrateQuery();
                LLayer.LL.sentquery();
                JOptionPane.showMessageDialog(AddRoom.this,"You have successfully added the room information!");
    		resortText.setText("");
    		roomText.setText("");
    		priceText.setText("");
            } catch (SQLException ex) {
                Logger.getLogger(AddClient.class.getName()).log(Level.SEVERE, null, ex);
            }			
 
		}
		
	}
}
