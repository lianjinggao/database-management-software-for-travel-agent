/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author xunshuairu
 */
public class QueryResortRoomType extends JPanel {
    private JTextField resortName = new JTextField(10);
    private JTextField roomTypeName = new JTextField(10);
    private JTextField price = new JTextField(10);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
    		
	
	public QueryResortRoomType() throws SQLException{
		
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("resorts_room_type",new Dimension(400,300));
        baseUpper.add(new JLabel("ID:"));
        baseUpper.add(resortName);
        baseUpper.add(new JLabel("Name:"));
        baseUpper.add(roomTypeName);


        baseUpper2.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper2);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);

        JPanel bot = new JPanel(new FlowLayout());
      if(LLayer.LL.getpriv()==true){

        }
        base.add(bot, BorderLayout.SOUTH);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
    	//table.a.setPreferredSize(new Dimension(600,300));
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
	public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("resorts_room_type");
            LLayer.LL.slots1.add(resortName.getText());
            LLayer.LL.slots1.add(roomTypeName.getText());
            LLayer.LL.slots1.add(price.getText());
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            }    
        }
    }	
    
}
