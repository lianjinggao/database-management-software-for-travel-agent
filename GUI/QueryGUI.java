package GUI;

import dbapp.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryGUI extends JFrame{
	
	private JPanel mid = new JPanel();
	private JPanel cardDefault = new JPanel();
	private QueryCity queryCity = new QueryCity();
	private QueryPerson queryPerson = new QueryPerson();
	private QueryAmenity queryAmenity = new QueryAmenity();
	private QueryResort queryResort = new QueryResort();
        
    private QueryResortRoomType queryResortRoomType = new QueryResortRoomType();
        
        
	private QueryRoomType queryRoomType = new QueryRoomType();
	private QueryBookingID queryBooking = new QueryBookingID();		
	private ComQueryBookingName queryBookingName = new ComQueryBookingName();
	//private QueryAgent queryAgent = new QueryAgent();
	private QueryEmail queryEmail = new QueryEmail();
	private QueryPhone queryPhone = new QueryPhone();
	private QueryBookingResort bookingResort = new QueryBookingResort();
	private ComQueryBookingResortName bookingResortName = new ComQueryBookingResortName();
	private Component clientTrip = new ComQueryClientTrip();
	private Component totalRevenueAgent = new ComQueryTotalRevenueAgent();
	private Component totalRevenueCity = new ComQueryTotalRevenueCity();
	private Component totalRevenueCountry = new ComQueryTotalRevenueCountry();
	private Component resortAmenityList = new ComQueryResortAmenityList();
	private Component resortAmenityCount = new ComQueryResortAmenityCount();
	private Component resortCity = new ComQueryResortCity();
	private Component resortCountry = new ComQueryResortCountry();
	private Component clientResortDays = new ComQueryClientResortDays();
	private Component clientResortDaysSum = new ComQueryClientResortDaysSum();
	private Component numberOfTrips = new ComQueryNumberOfTrips();
	private Component avgDays = new ComQueryAvgDaysPerTrip();
	private Component handledTrips = new ComQueryAgentHandledTrips();
	private Component handledTripsCount = new ComQueryAgentHandledTripsCount();
	private Component payment = new ComQueryBookingResortNamePayment();
	private Component handledClientCount = new ComQueryAgentHandledClientsCount();
	private Component resortAmenityMost = new ComQueryResortAmenityMost();
	private Component moneyspent = new ComQueryMoneySpent();
	private Component mosttrip = new ComQueryMostTrip();
	
	public QueryGUI() throws SQLException{
		
		this.setTitle("Query Interface");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		JPanel base = new JPanel();
		base.setLayout(new BorderLayout());
		
		//top
		JPanel top = new JPanel();
		top.setLayout(new BoxLayout(top, BoxLayout.LINE_AXIS));
		JMenuBar menuBar= new JMenuBar();
		JMenu menuQuery = new JMenu("SimpleQuery");
		JMenu menuComQuery = new JMenu("ComplexQuery");
		JMenu menuNew = new JMenu("New Window");
		menuQuery.setMnemonic(KeyEvent.VK_S);
		menuComQuery.setMnemonic(KeyEvent.VK_C);
		menuNew.setMnemonic(KeyEvent.VK_N);
		//SimpleQuery
		JMenuItem menuItemQueryClient = new JMenuItem("Person", KeyEvent.VK_C);
		JMenuItem menuItemQueryBooking = new JMenuItem("Booking Info + ID", KeyEvent.VK_B);
		JMenuItem menuItemQueryCity = new JMenuItem("City", KeyEvent.VK_I);
		JMenuItem menuItemQueryBookingResort = new JMenuItem("BookingResort Info + ID", KeyEvent.VK_K);
		JMenuItem menuItemQueryAmenity = new JMenuItem("Amenity", KeyEvent.VK_A);
		JMenuItem menuItemQueryResort = new JMenuItem("Resort", KeyEvent.VK_R);
                JMenuItem menuItemQueryResortRoomType = new JMenuItem("ResortRoomType", KeyEvent.VK_R);
                
                
		JMenuItem menuItemQueryRoomType = new JMenuItem("Room Type", KeyEvent.VK_T);
		//JMenuItem menuItemQueryAgent = new JMenuItem("Agent", KeyEvent.VK_G);
		JMenuItem menuItemQueryEmail = new JMenuItem("Email", KeyEvent.VK_E);
		JMenuItem menuItemQueryPhone = new JMenuItem("Phone", KeyEvent.VK_P);
		JMenuItem close = new JMenuItem("Close");
		JMenuItem close2 = new JMenuItem("Close");
		
		menuItemQueryClient.addActionListener(new ShowQueryClient());
		menuItemQueryCity.addActionListener(new ShowQueryCity());
		menuItemQueryAmenity.addActionListener(new ShowQueryAmenity());
		menuItemQueryResort.addActionListener(new ShowQueryResort());
                
                
                menuItemQueryResortRoomType.addActionListener(new ShowQueryResortRoomType());
                
                
		menuItemQueryRoomType.addActionListener(new ShowQueryRoomType());
		menuItemQueryBooking.addActionListener(new ShowQueryBooking());
		//menuItemQueryAgent.addActionListener(new ShowQueryAgent());
		menuItemQueryEmail.addActionListener(new ShowEmail());
		menuItemQueryPhone.addActionListener(new ShowPhone());
		menuItemQueryBookingResort.addActionListener(new ShowBookingResort());
		close.addActionListener(new Close());
		close2.addActionListener(new Close());
		
		menuQuery.add(menuItemQueryClient);
		menuQuery.add(menuItemQueryEmail);
		menuQuery.add(menuItemQueryPhone);
		menuQuery.addSeparator();
		menuQuery.add(menuItemQueryBooking);
		menuQuery.add(menuItemQueryBookingResort);
		menuQuery.addSeparator();
		menuQuery.add(menuItemQueryCity);
		menuQuery.add(menuItemQueryResort);
                menuQuery.add(menuItemQueryResortRoomType);
                
		menuQuery.add(menuItemQueryAmenity);
		menuQuery.add(menuItemQueryRoomType);
		menuQuery.addSeparator();
		//menuQuery.add(menuItemQueryAgent);
		//menuQuery.addSeparator();
		menuQuery.add(close);
		
		//ComplexQuery
		JMenuItem menuItemQueryBookingName = new JMenuItem("BookingName Info", KeyEvent.VK_B);
		JMenuItem menuItemQueryBookingResortName = new JMenuItem("BookingResort Name", KeyEvent.VK_K);
		JMenuItem menuItemQueryPayment = new JMenuItem("BookingResort + Payment",KeyEvent.VK_P);
		JMenuItem menuItemQueryClientTrip = new JMenuItem("Client Trip", KeyEvent.VK_C);
		JMenuItem menuItemQueryTotalRevenueAgent = new JMenuItem("Total Revenue by Agent", KeyEvent.VK_A);
		JMenuItem menuItemQueryTotalRevenueCity = new JMenuItem("Total Revenue by City", KeyEvent.VK_E);
		JMenuItem menuItemQueryTotalRevenueCountry = new JMenuItem("Total Revenue by Country", KeyEvent.VK_Y);
		JMenuItem menuItemQueryResortAmenityList = new JMenuItem("Resort Amenity List", KeyEvent.VK_L);
		JMenuItem menuItemQueryResortAmenityCount = new JMenuItem("Resort Amenity Count", KeyEvent.VK_T);
		JMenuItem menuItemQueryResortCity = new JMenuItem("Resort City", KeyEvent.VK_R);
		JMenuItem menuItemQueryResortCountry = new JMenuItem("Resort Country", KeyEvent.VK_E);
		JMenuItem menuItemQueryClientResortDays = new JMenuItem("Client Resort Days", KeyEvent.VK_D);
		JMenuItem menuItemQueryClientResortDaysSum = new JMenuItem("Client Resort Days Sum", KeyEvent.VK_U);
		JMenuItem menuItemQueryNumberOfTrips = new JMenuItem("Client Number Of Trips", KeyEvent.VK_N);
		JMenuItem menuItemQueryAvgDays = new JMenuItem("Average Days Per Trips", KeyEvent.VK_V);
		JMenuItem menuItemQueryHandledTrips = new JMenuItem("Agent Handled Trips");
		JMenuItem menuItemQueryHandledTripsCount = new JMenuItem("Agent Handled Trips Count");
		JMenuItem menuItemQueryHandledClientsCount = new JMenuItem("Agent Handled Cilents Count");
		JMenuItem menuItemQueryHandledClientsMost = new JMenuItem("Resort Amenity All");
		JMenuItem menuItemQueryMostSpent = new JMenuItem("Money Spent Per Trip");
		JMenuItem menuItemQueryMostTrip = new JMenuItem("Most Trip Per Person");
                
		menuItemQueryBookingName.addActionListener(new ShowQueryBookingName());
		menuItemQueryBookingResortName.addActionListener(new ShowQueryBookingResortName());
		menuItemQueryClientTrip.addActionListener(new ShowQueryClientTrip());
		menuItemQueryTotalRevenueAgent.addActionListener(new ShowTotalAgent());
		menuItemQueryTotalRevenueCity.addActionListener(new ShowTotalCity());
		menuItemQueryTotalRevenueCountry.addActionListener(new ShowTotalCountry());
		menuItemQueryResortAmenityList.addActionListener(new ShowResortAmenityList());
		menuItemQueryResortAmenityCount.addActionListener(new ShowResortAmenityCount());
		menuItemQueryResortCity.addActionListener(new ShowResortCity());
		menuItemQueryResortCountry.addActionListener(new ShowResortCountry());
		menuItemQueryClientResortDays.addActionListener(new ShowClientResortDays());
		menuItemQueryClientResortDaysSum.addActionListener(new ShowClientResortDaysSum());
		menuItemQueryNumberOfTrips.addActionListener(new ShowNumberOfTrips());
		menuItemQueryAvgDays.addActionListener(new ShowAvgDays());
		menuItemQueryHandledTrips.addActionListener(new ShowHandledTrips());
		menuItemQueryHandledTripsCount.addActionListener(new ShowHandledTripsCount());
		menuItemQueryPayment.addActionListener(new ShowPayment());
		menuItemQueryHandledClientsCount.addActionListener(new ShowHandledClientsCount());
		menuItemQueryHandledClientsMost.addActionListener(new ShowResortAmenityMost());
		menuItemQueryMostSpent.addActionListener(new ShowMoneySpent());
                menuItemQueryMostTrip.addActionListener(new ShowMostTrip());
                
                
		
		menuComQuery.add(menuItemQueryBookingName);
		menuComQuery.add(menuItemQueryBookingResortName);
		menuComQuery.add(menuItemQueryPayment);
		menuComQuery.addSeparator();
		menuComQuery.add(menuItemQueryClientTrip);
		menuComQuery.addSeparator();
		menuComQuery.add(menuItemQueryTotalRevenueAgent);
		menuComQuery.add(menuItemQueryTotalRevenueCity);
		menuComQuery.add(menuItemQueryTotalRevenueCountry);
		menuComQuery.addSeparator();
		menuComQuery.add(menuItemQueryResortAmenityList);
		menuComQuery.add(menuItemQueryResortAmenityCount);
		menuComQuery.add(menuItemQueryHandledClientsMost);
		menuComQuery.addSeparator();
		menuComQuery.add(menuItemQueryResortCity);
		menuComQuery.add(menuItemQueryResortCountry);
		menuComQuery.addSeparator();
		menuComQuery.add(menuItemQueryClientResortDays);
		menuComQuery.add(menuItemQueryClientResortDaysSum);
		menuComQuery.addSeparator();
		menuComQuery.add(menuItemQueryNumberOfTrips);
                menuComQuery.add(menuItemQueryMostTrip);
		menuComQuery.add(menuItemQueryMostSpent);
		menuComQuery.add(menuItemQueryAvgDays);
		menuComQuery.addSeparator();
		menuComQuery.add(menuItemQueryHandledTrips);
		menuComQuery.add(menuItemQueryHandledTripsCount);
		menuComQuery.add(menuItemQueryHandledClientsCount);
		menuComQuery.addSeparator();
		menuComQuery.add(close2);
		//New Window
		JMenuItem newWindow = new JMenuItem("Open A New Query Window", KeyEvent.VK_O);
		newWindow.addActionListener(new OpenWindow());
		menuNew.add(newWindow);
		//add to bar
		menuBar.add(menuQuery);
		menuBar.add(menuComQuery);
		menuBar.add(menuNew);
		top.add(menuBar);
		base.add(top, BorderLayout.PAGE_START);
		
		
		//mid
		mid.setLayout(new CardLayout());
		
		cardDefault.add(new JLabel("Welcome!"));
		cardDefault.add(new JLabel("Please select operations on the menu."));
		cardDefault.add(new JLabel("Thank you!"));
		
		mid.add(cardDefault, "QueryDefault");
		mid.add(queryPerson, "QueryPerson");
		mid.add(queryBooking, "QueryBooking");
		mid.add(queryBookingName, "QueryBookingName");
		mid.add(queryCity,"QueryCity");
		mid.add(queryResort, "QueryResort");
                mid.add(queryResortRoomType,"QueryResortRoomType");
                
                
		mid.add(queryAmenity, "QueryAmenity");
		mid.add(queryRoomType, "QueryRoomType");
		//mid.add(queryAgent, "QueryAgent");
		mid.add(queryEmail, "QueryEmail");
		mid.add(queryPhone,"QueryPhone");
		mid.add(bookingResort, "QueryBookingResort");
		mid.add(bookingResortName, "QueryBookingResortName");
		mid.add(clientTrip, "QueryClientTrip");
		mid.add(totalRevenueAgent,"ShowTotalAgent");
		mid.add(totalRevenueCity,"ShowTotalCity");
		mid.add(totalRevenueCountry,"ShowTotalCountry");
		mid.add(resortAmenityList,"ShowResortAmenityList");
		mid.add(resortAmenityCount,"ShowResortAmenityCount");
		mid.add(resortAmenityMost,"ShowResortAmenityMost");
		mid.add(resortCity,"ShowResortCity");
		mid.add(resortCountry,"ShowResortCountry");
		mid.add(clientResortDays,"ShowClientResortDays");
		mid.add(clientResortDaysSum,"ShowClientResortDaysSum");
		mid.add(numberOfTrips,"ShowNumberOfTrips");
		mid.add(avgDays,"ShowAvgDays");
		mid.add(handledTrips,"ShowHandledTrips");
		mid.add(handledTripsCount,"ShowHandledTripsCount");
		mid.add(handledClientCount,"ShowHandledClientsCount");
		mid.add(payment,"ShowPayment");
		mid.add(moneyspent,"ShowMoneySpent");
		mid.add(mosttrip,"ShowMostTrip");
                
		base.add(mid, BorderLayout.CENTER);
		
		this.add(base);
		this.pack();
	    this.setLocationRelativeTo(null);
		this.setVisible(true);
	
	}
	
	private class ShowQueryClient extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryPerson");
			QueryGUI.this.setTitle("Person");
		}
	}
	
	private class ShowQueryCity extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryCity");
			QueryGUI.this.setTitle("City");
		}
	}
	
	private class ShowQueryAmenity extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryAmenity");
			QueryGUI.this.setTitle("Amenity");
		}
	}
	
	private class ShowQueryResort extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryResort");
			QueryGUI.this.setTitle("Resort");
		}
	}
        
        private class ShowQueryResortRoomType extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryResortRoomType");
			QueryGUI.this.setTitle("Query Resort Room Type");
		}
	}
	
	private class ShowQueryRoomType extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryRoomType");
			QueryGUI.this.setTitle("Room Type");
		}
	}
	
	private class ShowQueryBooking extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryBooking");
			QueryGUI.this.setTitle("Booking Info");
		}
	}
	
	private class ShowQueryBookingName extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryBookingName");
			QueryGUI.this.setTitle("Booking Info + Name");
		}
	}
	
	private class ShowQueryAgent extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryAgent");
			QueryGUI.this.setTitle("Agent");
		}
	}
	
	private class ShowEmail extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryEmail");
			QueryGUI.this.setTitle("Email");
		}
	}
	
	private class ShowPhone extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryPhone");
			QueryGUI.this.setTitle("Phone");
		}
	}
	private class ShowBookingResort extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryBookingResort");
			QueryGUI.this.setTitle("BookingResort Info");
		}
	}
	
	private class ShowQueryBookingResortName extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryBookingResortName");
			QueryGUI.this.setTitle("BookingResort Info + Name");
		}
	}
	
	private class ShowQueryClientTrip extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "QueryClientTrip");
			QueryGUI.this.setTitle("Client Trip");
		}
	}
	
	private class ShowTotalAgent extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowTotalAgent");
			QueryGUI.this.setTitle("Total Revenue by Agent");
		}
	}
	
	private class ShowTotalCity extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowTotalCity");
			QueryGUI.this.setTitle("Total Revenue by City");
		}
	}
	
	private class ShowTotalCountry extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowTotalCountry");
			QueryGUI.this.setTitle("Total Revenue by Country");
		}
	}
	
	private class ShowResortAmenityList extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowResortAmenityList");
			QueryGUI.this.setTitle("Resort Amenity List");
		}
	}
	
	private class ShowResortAmenityCount extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowResortAmenityCount");
			QueryGUI.this.setTitle("Resort Amenity Count");
		}
	}
	
	private class ShowResortCity extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowResortCity");
			QueryGUI.this.setTitle("Resort City");
		}
	}
	
	private class ShowResortCountry extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowResortCountry");
			QueryGUI.this.setTitle("Resort Country");
		}
	}
	
	private class ShowClientResortDays extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowClientResortDays");
			QueryGUI.this.setTitle("Client Resort Days");
		}
	}
	
	private class ShowClientResortDaysSum extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowClientResortDaysSum");
			QueryGUI.this.setTitle("Client Resort Days Sum");
		}
	}
	
	private class ShowNumberOfTrips extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowNumberOfTrips");
			QueryGUI.this.setTitle("Client Number of Trips");
		}
	}
	
	private class ShowAvgDays extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowAvgDays");
			QueryGUI.this.setTitle("Average Days Per Trip");
		}
	}
	
	private class ShowHandledTrips extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowHandledTrips");
			QueryGUI.this.setTitle("Agent Handled Trips");
		}
	}
	
	private class ShowHandledTripsCount extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowHandledTripsCount");
			QueryGUI.this.setTitle("Agent Handled Trips Count");
		}
	}
	private class ShowPayment extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowPayment");
			QueryGUI.this.setTitle("Booking Resort + Payment");
		}
	}
	
	private class ShowHandledClientsCount extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowHandledClientsCount");
			QueryGUI.this.setTitle("Agent Handled Cilents Count");
		}
	}
	
	
	private class ShowResortAmenityMost extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowResortAmenityMost");
			QueryGUI.this.setTitle("Resort Amenity Most");
		}
	}
	
	private class ShowMoneySpent extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowMoneySpent");
			QueryGUI.this.setTitle("Money Spent Per Trip");
		}
	}
        
        
        private class ShowMostTrip extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			CardLayout a = (CardLayout) mid.getLayout();
			a.show(mid, "ShowMostTrip");
			QueryGUI.this.setTitle("Most Trip Per Person");
		}
	}
	
	private class Close extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			QueryGUI.this.dispose();
		}
	}
	
	private class OpenWindow extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				QueryGUI windows = new QueryGUI();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
//			catch (SQLException ex) {
//                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
