package GUI;
import dbapp.LLayer;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SignUp extends JFrame{
	
	  // int id;
	   private JPasswordField password;
       private JTextField firstNameText, lastNameText, yearText, monthText, dayText, zipcodeText;
       private JRadioButton femaleBox, maleBox;
       private JRadioButton agentBox, managerBox;
       private JTextField phoneText1, phoneText2, emailText1, emailText2;
        
	public SignUp() {
		
		this.setTitle("Sign Up");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//base
		JPanel base = new JPanel(new BorderLayout());
		
		//top
		JPanel top = new JPanel(new FlowLayout());
		top.add(new JLabel("Password:"));
		password = new JPasswordField(10);
		top.add(password);
		base.add(top, BorderLayout.NORTH);
		
		//left
		JPanel left = new JPanel();
		left.setLayout(new BoxLayout(left, BoxLayout.PAGE_AXIS));
		
		JPanel secondRow = new JPanel(new FlowLayout());
		JPanel thirdRow = new JPanel(new FlowLayout());
		JPanel fourthRow = new JPanel(new FlowLayout());
		JPanel fifthRow = new JPanel(new FlowLayout());
		
		JLabel firstNameLabel = new JLabel("First_Name:");
		JLabel lastNameLabel = new JLabel("Last_Name:");
		JLabel dobLabel = new JLabel("Day of Birth:");
		JLabel yearLabel = new JLabel("Year:");
		JLabel monthLabel = new JLabel("Month:");
		JLabel dayLabel = new JLabel("Day:");
		JLabel genderLabel = new JLabel("Gender:");
		JLabel femaleLabel = new JLabel("Female:");
		JLabel maleLabel = new JLabel("Male:");
		JLabel zipcodeLabel = new JLabel("Zipcode:");
		
		firstNameText = new JTextField(10);
		lastNameText = new JTextField(10);
		yearText = new JTextField(4);
		monthText = new JTextField(2);
		dayText = new JTextField(2);
		zipcodeText = new JTextField(5);
		
		femaleBox = new JRadioButton();
        femaleBox.addActionListener(new femaleaction());
                
		maleBox = new JRadioButton();
        maleBox.addActionListener(new maleaction());
        maleBox.setSelected(true);
				
		secondRow.add(firstNameLabel);
		secondRow.add(firstNameText);
		secondRow.add(lastNameLabel);
		secondRow.add(lastNameText);
		
		thirdRow.add(dobLabel);
		thirdRow.add(yearLabel);
		thirdRow.add(yearText);
		thirdRow.add(monthLabel);
		thirdRow.add(monthText);
		thirdRow.add(dayLabel);
		thirdRow.add(dayText);
		
		fourthRow.add(genderLabel);
		fourthRow.add(femaleBox);
		fourthRow.add(femaleLabel);
		fourthRow.add(maleBox);
		fourthRow.add(maleLabel);
		
		fifthRow.add(zipcodeLabel);
		fifthRow.add(zipcodeText);
		
		left.add(secondRow);
		left.add(thirdRow);
		left.add(fourthRow);
		left.add(fifthRow);
		
		base.add(left ,BorderLayout.LINE_START);
		
		//center
		JPanel center = new JPanel();
		center.setLayout(new GridLayout(0,1));
		
		JLabel phoneLabel = new JLabel("Phone:");
		JLabel emailLabel = new JLabel("Email:");
		JLabel agentLabel = new JLabel("Agent:");
		JLabel managerLabel = new JLabel("Manager:");
		
		agentBox = new JRadioButton();
		managerBox = new JRadioButton();
		agentBox.setSelected(true);
		agentBox.addActionListener(new agentaction());
		managerBox.addActionListener(new manageraction());
		
		center.add(phoneLabel);
		center.add(new JLabel());
		center.add(emailLabel);
		center.add(new JLabel());
		center.add(agentBox);
		center.add(managerBox);
		
		base.add(center, BorderLayout.CENTER);
		
		//right
		JPanel right = new JPanel();
		right.setLayout(new GridLayout(0,1));

		phoneText1 = new JTextField(10);
		phoneText2 = new JTextField(10);
		emailText1 = new JTextField(20);
		emailText2 = new JTextField(20);

		right.add(phoneText1);
		right.add(phoneText2);
		right.add(emailText1);
		right.add(emailText2);
		right.add(agentLabel);
		right.add(managerLabel);
		
		base.add(right, BorderLayout.LINE_END);

		//bot
		JPanel bot = new JPanel(new FlowLayout());
		JButton submit = new JButton("Submit");
                submit.addActionListener(new addAction());
		bot.add(submit);

		base.add(bot, BorderLayout.PAGE_END);
		
		this.add(base);
		this.pack();
	    this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
    private class addAction extends AbstractAction{
        @Override
        public void actionPerformed(ActionEvent e) {
        		LLayer.LL.setStateFresh("addclient");
        		
        	   LLayer.LL.name_slot3=password.getText();
        	   LLayer.LL.name_slot1=firstNameText.getText();
               LLayer.LL.name_slot2=lastNameText.getText(); 
               LLayer.LL.date_slot1=monthText.getText()+"-"+dayText.getText()+"-"+yearText.getText();
               if(maleBox.isSelected()){
                    LLayer.LL.single_slot1 = "M";
               }
               else{
                   LLayer.LL.single_slot1 = "F";
               }
               LLayer.LL.zipCode_slot = zipcodeText.getText();
               if(agentBox.isSelected()){
                    LLayer.LL.single_slot2 = "A";
               }
               if(managerBox.isSelected()){
                   LLayer.LL.single_slot2 = "M";
               }
               if(!phoneText1.getText().equals(""))
            	   LLayer.LL.phone_slot = phoneText1.getText();
               if(!phoneText2.getText().equals(""))
            	   LLayer.LL.phone_slot2 = phoneText2.getText();
               if(!emailText1.getText().equals(""))
            	   LLayer.LL.email_slot1 = emailText1.getText();
               if(!emailText2.getText().equals(""))
            	   LLayer.LL.email_slot2 = emailText2.getText();
            try {
            	if(password.getPassword() == null || password.getPassword().length == 0){
            		JOptionPane.showMessageDialog(SignUp.this,"Password cannot be empty.");
                    return;
            	}	
            	if(LLayer.LL.checkInfo()==false){
                    JOptionPane.showMessageDialog(SignUp.this,"Names cannot be empty and should only contain letters.");
                    return;
                    }
                LLayer.LL.genrateQuery();
                LLayer.LL.sentquery();
                JOptionPane.showMessageDialog(SignUp.this,"You have successfully registered!");
                firstNameText.setText("");
                lastNameText.setText("");
                monthText.setText("");
                dayText.setText("");
                yearText.setText("");
                zipcodeText.setText("");
                phoneText1.setText("");
                phoneText2.setText("");
                emailText1.setText("");
                emailText2.setText("");
            } catch (SQLException ex) {
                Logger.getLogger(AddClient.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }

    private class femaleaction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(femaleBox.isSelected())
				maleBox.setSelected(false);
			else
				maleBox.setSelected(true);
		}
	}
	
	private class maleaction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(maleBox.isSelected())
				femaleBox.setSelected(false);
			else
				femaleBox.setSelected(true);
		}
	}
	
	private class agentaction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(agentBox.isSelected())
				managerBox.setSelected(false);
			else
				managerBox.setSelected(true);
		}
	}
	private class manageraction extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(managerBox.isSelected())
				agentBox.setSelected(false);
			else
				agentBox.setSelected(true);
		}		
	}
}
