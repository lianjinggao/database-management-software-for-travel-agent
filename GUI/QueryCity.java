/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xunshuairu
 */
public class QueryCity extends JPanel{

	
    private JTextField cityText = new JTextField(10);
    
    private JTextField countryText = new JTextField(10);
    
    private JPanel base = new JPanel(new BorderLayout()); 
    private JPanel baseUpper = new JPanel(new FlowLayout()); 
    private JPanel baseRight = new JPanel(new BorderLayout());
    
    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
    private JButton updateButton = new JButton("update");
    private JButton deleteButton = new JButton("delete");
	
public QueryCity() throws SQLException{
		
	JPanel base = new JPanel(new BorderLayout()); 
	JPanel baseUpper = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("querycity",GridTable.DEFAULT_DIMENSION);
        baseUpper.add(new JLabel("City:"));
        baseUpper.add(cityText);
        baseUpper.add(new JLabel("Country:"));
        baseUpper.add(countryText);
        baseUpper.add(searchButton);
        base.add(baseUpper, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
        
        updateButton.addActionListener(new Update());
        
        deleteButton.addActionListener(new Delete());
        
        
        JPanel bot = new JPanel(new FlowLayout());
        
        if(LLayer.LL.getpriv()==true){
            bot.add(updateButton);
            bot.add(deleteButton);
        }
        base.add(bot, BorderLayout.SOUTH);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }
        private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                    LLayer.LL.setStateFresh("querycity");
                    LLayer.LL.slots1.add(cityText.getText());
                    LLayer.LL.slots1.add(countryText.getText());
                    try {
                        table.search();
                        refresh();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    }    
                }
	}
        
         private class Update extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                   LLayer.LL.setStateFresh("querycity");
                    try {
                        table.update();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    }
            		JOptionPane.showMessageDialog(QueryCity.this,"You have successfully updated the information! " +
            				"You can press Search/Refersh to see the current result.");
                }
	}
         
        private class Delete extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
                   LLayer.LL.setStateFresh("querycity");
                    try {
                        table.delete();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    JOptionPane.showMessageDialog(QueryCity.this,"You have successfully deleted the information! " +
            				"You can press Search/Refersh to see the current result.");
                }
	}
}
