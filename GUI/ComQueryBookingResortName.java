package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryBookingResortName extends JPanel
{
	private JTextField trip_id = new JTextField(5);
	private JTextField cilentFirstName = new JTextField(5);
    private JTextField cilentLastName = new JTextField(5);
    private JTextField resortName = new JTextField(10);
    private JTextField room_type_name = new JTextField(5);
    private JTextField room_quantity = new JTextField(2);
    private JTextField arrival_date = new JTextField(6);
    private JTextField departure_date = new JTextField(6);
    private JTextField arrival_date_to = new JTextField(6);
    private JTextField departure_date_to = new JTextField(6);
    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");

	
	public ComQueryBookingResortName() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout());
	    JPanel baseUpper1_5 = new JPanel(new FlowLayout());
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("bookingresortname",0, new Dimension(700,300));
        baseUpper.add(new JLabel("Trip ID:"));
        baseUpper.add(trip_id);
        baseUpper.add(new JLabel("Client First Name:"));
        baseUpper.add(cilentFirstName);
        baseUpper.add(new JLabel("Client Last Name:"));
        baseUpper.add(cilentLastName);
 
        baseUpper1_5.add(new JLabel("Resort Name:"));
        baseUpper1_5.add(resortName);
        baseUpper1_5.add(new JLabel("Room Type:"));
        baseUpper1_5.add(room_type_name);
        baseUpper1_5.add(new JLabel("Room Quantity:"));
        baseUpper1_5.add(room_quantity);
        
        baseUpper2.add(new JLabel("Arrival Time From:"));
        baseUpper2.add(arrival_date);
        baseUpper2.add(new JLabel("Arrival Time To:"));
        baseUpper2.add(arrival_date_to);
        baseUpper2.add(new JLabel("Departure Time From:"));
        baseUpper2.add(departure_date);
        baseUpper2.add(new JLabel("Departure Time To:"));
        baseUpper2.add(departure_date_to);
        
        baseUpper3.add(new JLabel("Date Format: yyyy-mm-dd            "));
        baseUpper3.add(searchButton);
        up.add(baseUpper);
        up.add(baseUpper1_5);
        up.add(baseUpper2);
        up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                        
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
                 LLayer.LL.setStateFresh("bookingresortname");
                 
                    LLayer.LL.slots1.add(trip_id.getText());
                    LLayer.LL.slots1.add(cilentFirstName .getText());
                    LLayer.LL.slots1.add(cilentLastName.getText());
                    LLayer.LL.slots1.add(resortName.getText());
                    LLayer.LL.slots1.add(room_type_name .getText());
                    LLayer.LL.slots1.add(room_quantity.getText());
                    LLayer.LL.slots1.add(arrival_date.getText());
                    LLayer.LL.slots1.add(arrival_date_to.getText());
                    LLayer.LL.slots1.add(departure_date.getText());
                    LLayer.LL.slots1.add(departure_date_to.getText());
                    try {
                        table.search();
                        refresh();
                    } catch (SQLException ex) {
                        Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
                    }    
        }
    }	
}