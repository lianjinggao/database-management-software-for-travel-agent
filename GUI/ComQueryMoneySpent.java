package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryMoneySpent extends JPanel 
{
	private JTextField id = new JTextField(5);
    private JTextField cilentFirstName = new JTextField(5);
    private JTextField cilentLastName = new JTextField(5);
    private JTextField bookingDate = new JTextField(6);
    private JTextField bookingDateTo = new JTextField(6);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
	
	public ComQueryMoneySpent() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper2 = new JPanel(new FlowLayout()); 
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("money_spent_per_trip",2, new Dimension(700,300));
        
        
        
        
//       baseUpper.add(new JLabel("Client ID:"));
//       baseUpper.add(id);
//       baseUpper.add(new JLabel("Client First Name:"));
//     baseUpper.add(cilentFirstName);
//        baseUpper.add(new JLabel("Client Last Name:"));
//     baseUpper.add(cilentLastName);
        
        baseUpper2.add(new JLabel("Booking Date From:"));
        baseUpper2.add(bookingDate);
        baseUpper2.add(new JLabel("Booking Date To:"));
        baseUpper2.add(bookingDateTo);
        
        baseUpper3.add(new JLabel("Date Format: yyyy-mm-dd            "));
        baseUpper3.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper2);
        up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("money_spent_per_trip");
            
            
            LLayer.LL.id_slot1=id.getText();
            LLayer.LL.name_slot1=cilentFirstName.getText();
            LLayer.LL.name_slot2=cilentLastName.getText();
            LLayer.LL.date_slot1 = bookingDate.getText();
            LLayer.LL.date_slot2 = bookingDateTo.getText();
           
            try {
                LLayer.LL.specialcase1sql();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }	
}