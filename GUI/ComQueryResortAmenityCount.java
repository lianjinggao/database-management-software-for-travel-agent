package GUI;

import javax.swing.*;

import dbapp.LLayer;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComQueryResortAmenityCount extends JPanel 
{
	private JTextField resort_name = new JTextField(5);
    private JTextField number = new JTextField(5);

    private GridTable table;
    
    private JButton searchButton = new JButton("Search/Refresh");
	
	public ComQueryResortAmenityCount() throws SQLException{
		
		JPanel base = new JPanel(new BorderLayout()); 
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
	    JPanel baseUpper = new JPanel(new FlowLayout()); 
	    JPanel baseUpper3 = new JPanel(new FlowLayout()); 
	    
        searchButton.addActionListener(new Search());
        table = new GridTable("resortamenitycount",0, new Dimension(400,300));
        baseUpper.add(new JLabel("Resort Name"));
        baseUpper.add(resort_name);
        baseUpper.add(new JLabel("Number of Amenity:"));
        baseUpper.add(number);

        baseUpper3.add(searchButton);
        
        up.add(baseUpper);
        up.add(baseUpper3);
        
        base.add(up, BorderLayout.NORTH);
        base.add(table, BorderLayout.CENTER);
                
		this.add(base);
	}
        
    public void refresh() throws SQLException{
    	table.refresh();
        this.updateUI();
    }

    private class Search extends AbstractAction{
		@Override
		public void actionPerformed(ActionEvent e) {
            LLayer.LL.setStateFresh("resortamenitycount");
            
            LLayer.LL.slots1.add(resort_name.getText());
            LLayer.LL.slots1.add(number.getText());
            try {
                table.search();
                refresh();
            } catch (SQLException ex) {
                Logger.getLogger(QueryCity.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }	
}